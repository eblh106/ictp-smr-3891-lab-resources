#!/usr/bin/python3

from audioop import add
import numpy as np
from numpy.lib.function_base import average
from scipy import stats
from functools import wraps
from time import time
import matplotlib.pyplot as plt
import scipy
import statistics

'''
Pearson correlation performance comparison
    - Scipy
    - "Bare-metal" algorithm in Python
    - Optimized pearson algorithm
'''


'''
Timing decorator to measure performance
'''
def timeIt(func):
    @wraps(func)
    def _time_it(*args, **kwargs):
        start = int(round(time() * 1000))
        try:
            return func(*args, **kwargs)
        finally:
            end_ = int(round(time() * 1000)) - start
            print(f"Total execution time of {func}: {end_ if end_ > 0 else 0} ms")
    return _time_it




class PearsonComparison(object):
    USING_BAREMETAL = 0
    USING_NUMPY = 1
    
    def __init__(self, bareMetalMode = USING_BAREMETAL):
        self.bareMetalMode = bareMetalMode
    
    
    '''
    Public methods
    '''
    
    @timeIt
    def benchmarkScipy(self, x, y, N):
        for i in range(N):
            _ = self.pearsonScipy(x, y)
            
            
    @timeIt
    def benchmarkBareMetal(self, x, y, N):
        for i in range(N):
            _ = self.pearsonBareMetal(x, y)
            
    @timeIt
    def benchmarkOptimized(self, x, y, N):
        for i in range(N):
            _ = self.pearsonModified(x, y)
            
    
    # @timeIt
    def pearsonScipy(self, x, y):
        return stats.pearsonr(x, y)[0]
    
    
    # @timeIt
    def pearsonBareMetal(self, x, y):
        productOfMeans = 0.0
        xMean = self.computeMean(x)
        yMean = self.computeMean(y)
        xStd = self.computeStdDev(x)
        yStd = self.computeStdDev(y)
        
        for i in range(len(x)):
            productOfMeans += (x[i] - xMean)*(y[i] - yMean)
        
        return (productOfMeans / (xStd * yStd)) 
    
    
    # @timeIt
    def pearsonModified(self, x, y):
        
        # sqrt(2/pi) normalization for mean absolute deviation
        NORMALIZATION_CONSTANT = np.sqrt(2.0/np.pi)
        
        productOfMeans = 0.0
        xMean = self.computeMean(x)
        yMean = self.computeMean(y)
        xAbsMeanDev = self.computeAbsMeanDeviation(x)
        yAbsMeanDev = self.computeAbsMeanDeviation(y)

        result = float(0.0)

        for i in range(len(x)):
            productOfMeans += (x[i] - xMean)*(y[i] - yMean)
            
            xScore = (x[i] - xMean)
            yScore = (y[i] - yMean)
            
            result += xScore*yScore
        
        # return (productOfMeans / (xAbsMeanDev * yAbsMeanDev))* len(x) * NORMALIZATION_CONSTANT 
        # result = (productOfMeans / (xAbsMeanDev * yAbsMeanDev)) * NORMALIZATION_CONSTANT 
        
        result = result / (xAbsMeanDev * yAbsMeanDev)
        
        result = np.array(result)

        result *= NORMALIZATION_CONSTANT

        # result = result / 2.0
        
        return result



    '''
    Private methods
    '''
    def computeMean(self, data):
        if self.bareMetalMode == self.USING_BAREMETAL:
            return sum(data)/len(data)
        
        elif self.bareMetalMode == self.USING_NUMPY:
            return np.mean(data)
        
        else:
            return None
                
    
    def computeStdDev(self, data):
        stdVal = 0.0
        meanVal = self.computeMean(data)
        if self.bareMetalMode == self.USING_BAREMETAL:
            for i in data:
                stdVal += (i - meanVal)**2.0
            return (stdVal)**(0.5)

        elif self.bareMetalMode == self.USING_NUMPY:
            return np.std(data)*((len(data))**0.5)
        
        else:
            return None
        
    def computeAbsMeanDeviation(self, data):
        stdVal = 0.0
        meanVal = self.computeMean(data)
        for i in data:
            stdVal += abs(i - meanVal)
        
        return stdVal/np.sqrt(len(data))

class ej276Pulse(object):
    
    def __init__(self):
        self.tauList = []
        self.ampList = []  
        
    def setTauValues(self, tauList):
        self.tauList = tauList.copy()
        
    def setAmpValues(self, ampList):
        self.ampList = ampList.copy()
        
    def getTauValues(self):
        return self.tauList
    
    def getAmpValues(self):
        return self.ampList
    

class PulseSynthesis(object):   
    
    def __init__(self):
        self.ej276GammaConstants = ej276Pulse()
        self.ej276NeutronConstants = ej276Pulse()
        
        # Model from: "Fast neutron and gamma ray pulse shape discrimination in EJ-276 and EJ-276G plastic scintillators"
        
        # Time constants from scintillator characterization for gamma pulse
        TAU1_G = 4.0e-9
        TAU2_G = 16.0e-9
        TAU3_G = 98e-9
        TAU4_G = 690e-9

        # Time constants from scintillator characterization for neutron pulse
        TAU1_N = 3.9e-9
        TAU2_N = 18.0e-9
        TAU3_N = 106e-9
        TAU4_N = 800e-9
        
        # Amplitude proportions from scintillator characterization for gamma pulse
        AMP1_G = 0.71
        AMP2_G = 0.12
        AMP3_G = 0.08
        AMP4_G = 0.09

        # Amplitude proportions from scintillator characterization for neutron pulse
        AMP1_N = 0.47
        AMP2_N = 0.13
        AMP3_N = 0.13
        AMP4_N = 0.27        
        
        # Gamma constants initialization
        self.ej276GammaConstants.setTauValues([TAU1_G, TAU2_G, TAU3_G, TAU4_G])
        self.ej276GammaConstants.setAmpValues([AMP1_G, AMP2_G, AMP3_G, AMP4_G])
        
        # Custom amplitude values that represent the plot in the paper, not the reported parameters
        # self.ej276GammaConstants.setAmpValues([0.805, 0.1895, 0.0046, 0.0009]) 
        
        # Neutron constants initialization
        self.ej276NeutronConstants.setTauValues([TAU1_N, TAU2_N, TAU3_N, TAU4_N])
        self.ej276NeutronConstants.setAmpValues([AMP1_N, AMP2_N, AMP3_N, AMP4_N])
        
        # Custom amplitude values that represent the plot in the paper, not the reported parameters
        # self.ej276NeutronConstants.setAmpValues([0.845, 0.13, 0.02, 0.005])

        
               
        
    
    def pulseNoiseGen(self, noiseLen, noiseSigma):
        return np.random.normal(loc = 0, scale = noiseSigma, size = noiseLen)

    def quantize(self, pulse, quantizationLevels):
        return np.round(pulse * quantizationLevels) / quantizationLevels

    # Single "Dirac-Delta" pulse with defined length and amplitude. Pulse located at center
    def deltaPulse(self, length, amplitude, noiseSigma = 0, leadingProportion = 0.5, quantizationBits = 0):
        noise = self.pulseNoiseGen(length, noiseSigma)
        y1 = np.array([0]*int(length//2 - 1))
        y2 = np.array([float(amplitude)])
        y3 = np.array([0]*int(length//2))
        ySignal = np.concatenate((y1, y2, y3))
        
        if quantizationBits:
            return self.quantize(ySignal + noise, 2**quantizationBits)
        
        return ySignal + noise
    
    # Single triangular pulse with defined length and amplitude.
    def triangularPulse(self, length, amplitude, noiseSigma = 0, leadingProportion = 0.5, quantizationBits = 0):
        noise = self.pulseNoiseGen(length, noiseSigma)
        y1 = []
        y2 = []
        halfPulseLength = int(length/2)
        for i in range(halfPulseLength):
            y1.append(i*float(amplitude)/halfPulseLength)
        
        for i in range(halfPulseLength, int(length)):
            y2.append(float(amplitude) - (i - halfPulseLength)*float(amplitude)/halfPulseLength)
        
        ySignal = np.concatenate((y1, y2)) + noise
                
        if quantizationBits:
            ySignal = self.quantize(ySignal + noise, 2**quantizationBits)
        
        return ySignal
    
    # Single square pulse with defined length and amplitude. 50% duty cycle
    def squaredPulse(self, length, amplitude, noiseSigma = 0, leadingProportion = 0.5, quantizationBits = 0):
        noise = self.pulseNoiseGen(length, noiseSigma)
        y1 = np.array([float(amplitude)]*int((length*leadingProportion)))
        y2 = np.array([0.0]*int(length*(1 - leadingProportion)))
        ySignal = np.concatenate((y1, y2))

        if len(ySignal) != len(noise):
            ySignal = np.concatenate(([float(amplitude)], ySignal))
        
        ySignal += noise

        if quantizationBits:
            ySignal = self.quantize(ySignal + noise, 2**quantizationBits)
            
        return ySignal

    
    # Single PMT pulse output simulator, including noise and shaping parameters
    def pmtPulse(self, length, amplitude, noiseSigma = 0, leadingProportion = 0.02, quantizationBits = 0, samplingPeriod = 1.0e-9):
        t = np.arange(length)
        noise = self.pulseNoiseGen(length, noiseSigma)
        tau1 = int(leadingProportion*len(t)/5) 
        tau2 = int((1 - leadingProportion)*len(t)/5)
        y1 = np.array(0 - np.exp(-t/tau1))
        y2 = np.array(np.exp(-t/tau2))
        
        yPmt = y1 + y2 + noise
        
        yPmt -= min(yPmt)
        
        yPmt = yPmt / max(yPmt)
        yPmt *= amplitude
        
        if quantizationBits:
            yPmt = self.quantize(yPmt, 2**quantizationBits)
            
        return yPmt  

    # EJ-276 model pulse based on custom decay parameters
    def ej276CustomPulse(self, length, pulseParametersInstance,
                        amplitude = 1, noiseSigma = 0,
                        quantizationBits = 0, verticalOffset = 0,
                        samplingPeriod = 2.0e-9, risingEdgeProportion = 0,
                        addInitialZero = False):
        
        noise = self.pulseNoiseGen(length, noiseSigma)

        # Quad exponential decay model
        y = self.ej276PulseModel(
                tauList = pulseParametersInstance.getTauValues(),
                ampList = pulseParametersInstance.getAmpValues(),
                length = length,
                samplingPeriod = samplingPeriod,
                risingEdgeProportion = risingEdgeProportion,
                addInitialZero = addInitialZero
            )

        y *= amplitude
        y += noise
        
        y += verticalOffset
        
        if quantizationBits:
            y = self.quantize(y, 2**quantizationBits)
            
        return y
    
    
    # EJ-276 model pulse for Gamma
    def ej276GammaPulse(self, length, amplitude = 1, noiseSigma = 0,
                        quantizationBits = 0, verticalOffset = 0,
                        samplingPeriod = 1.0e-9, leadingProportion = 0,
                        addInitialZero = False):

        # Model from: "Fast neutron and gamma ray pulse shape discrimination in EJ-276 and EJ-276G plastic scintillators" 
        return self.ej276CustomPulse(length = length,
            pulseParametersInstance = self.ej276GammaConstants,
            amplitude = amplitude,
            noiseSigma = noiseSigma,
            quantizationBits = quantizationBits,
            verticalOffset = verticalOffset,
            samplingPeriod = samplingPeriod,
            risingEdgeProportion = leadingProportion,
            addInitialZero = addInitialZero
            )

    
    
    # EJ-276 model pulse for Neutron
    def ej276NeutronPulse(self, length, amplitude = 1, noiseSigma = 0,
                          quantizationBits = 0, verticalOffset = 0,
                          samplingPeriod = 1.0e-9, leadingProportion = 0,
                          addInitialZero = False):
        
        # Model from: "Fast neutron and gamma ray pulse shape discrimination in EJ-276 and EJ-276G plastic scintillators" 
        return self.ej276CustomPulse(length = length,
            pulseParametersInstance = self.ej276NeutronConstants,
            amplitude = amplitude,
            noiseSigma = noiseSigma,
            quantizationBits = quantizationBits,
            verticalOffset = verticalOffset,
            samplingPeriod = samplingPeriod,
            risingEdgeProportion = leadingProportion,
            addInitialZero = addInitialZero
            )
        
        
        
        
    # Base model for EJ-276 pulse, regardless of type of event (gamma or neutron)
    def ej276PulseModel(self, tauList, ampList, length, samplingPeriod = 2e-9, risingEdgeProportion = 0, addInitialZero = False):
        
        t = np.arange(length)
        tS = samplingPeriod
        
        if risingEdgeProportion > 0:
            TAU_PROPORTION = 1.1
            t1 = np.arange(int(risingEdgeProportion*length))
            t2 = np.arange(int((1-risingEdgeProportion)*length))
            
            # Add missing indices if missing due to truncation
            thisLen = len(t1) + len(t2)
            if thisLen < length:
                diff = length - thisLen
                for i in range(diff):
                    t2 = np.append(t2, t2[-1])
        
            risingA, risingTau = self.computeAmplitudeAndTau(peakPosition = t1[-1],
                                                             peakValue = 1,
                                                             tauProportion = TAU_PROPORTION)
            
            yRising = risingA*(np.exp(-t/risingTau) - np.exp(-t*TAU_PROPORTION/risingTau))
            
            yRising = yRising[:len(t1)]
            
        yFalling = 0
        for i in range(len(tauList)):
            yFalling += ampList[i]*np.exp(-t/(tauList[i]/tS))
            
        if risingEdgeProportion > 0:
            yFalling = yFalling[:len(t2)]
            y = np.append(yRising, yFalling)
        elif addInitialZero:
            y = np.append(0, yFalling[:-1])
        else:
            y = yFalling
        
            
        # y = 0
        
        
        # # Pulse model - decay
        # for i in range(len(tauList)):
        #     y += ampList[i]*np.exp(-t/(tauList[i]/tS))
        
        # y -= min(y)
        # y /= max(y)
        

        return y       
        
    # Compute exponential amplitude and decay constant parameters based on pulse model requirements
    # of peak location, peak amplitude value, and rise/decay proportion
    def computeAmplitudeAndTau(self, peakPosition, tauProportion, peakValue = 1):
        '''
        tau = peakPosition * (tauProportion - 1) / ln(p)
        
        A = peakValue / (e^(-t/tau) - e^(-pt/tau))
        
        tau1 = tauProportion * tau2
        '''
                
        t = peakPosition
        p = tauProportion
        y = peakValue 
        
        tau = t * (p - 1) / (np.log(p)/np.log(np.e))
        A = y / (np.exp(-t/tau) - np.exp(-p*t/tau))
        
        return [A, tau]
    
   
   
    # Compute an individual decay component of a multi-exponential pulse model
    def computeExponentialComponent(self, tau, amp, length, samplingPeriod = 2.0e-9, verticalOffset = 0):
        t = np.arange(length)
        tS = samplingPeriod
        
        y = 0
        y += amp*np.exp(-t/(tau/tS))
        
        y += verticalOffset
        
        return y
    
    
    
    # ecal2 pulse model
    def ecal2Pulse(self, length, amplitude, tau, timeOfArrival = 0, 
                   reflectionAmplitude = 0, reflectionDelay = 0,
                   noiseSigma = 0, verticalOffset = 0, quantizationBits = 0):
        
        # yEcal2 -= min(yEcal2)
        # yEcal2 /= max(yEcal2)
        # yEcal2 *= amplitude

            
        t = np.arange(length)
        yEcal2 = self.math_model_ecal2(x = t, t0 = timeOfArrival, amp = amplitude,
                                       tau = tau, offset = 0, k = reflectionAmplitude,
                                       dt = reflectionDelay)
        
        
        noise = self.pulseNoiseGen(length, noiseSigma)
        yEcal2 += noise
        
        yEcal2 += verticalOffset
        
        if quantizationBits:
            yEcal2 = self.quantize(yEcal2, 2**quantizationBits)
            
        return yEcal2
    
    
    def ecal2PulseFromSamples(self, length = None, amplitude = 1, tau = None, timeOfArrival = None, 
                   reflectionAmplitude = None, reflectionDelay = None,
                   noiseSigma = 0, verticalOffset = 0, quantizationBits = 0):
        
        # No parameters required, since raw samples are provided. Just added for compatibility
        
        pulse = np.array([
        -0.000687086807184,
        0.008520670598785,
        0.251380784247534,
        0.791768714079825,
        1,
        0.904647587820646,
        0.766260656400035,
        0.628358882721173,
        0.485693890869108,
        0.361941068027922,
        0.271411924876739,
        0.201050807646745,
        0.151212971454626,
        0.111058671601825,
        0.085669775315522,
        0.064419828253134,
        0.049710382237799,
        0.036539316391851,
        0.030581798301236,
        0.022191782933879,
        0.01922224072187,
        0.013344485871032,
        0.01323261151964,
        0.009489869960294,
        0.00920299792312]
        )
        
        pulse *= amplitude
        pulse += verticalOffset
        
        return pulse

    
    

    def math_model_ecal2(self, x, t0: float, amp: float, tau: float, offset: float, k: float, dt: float):
        """The mathematical model of the semi-Gaussian pulse of ECAL2
        This model includes the pulse plus a reflection and the CSA is modeled as an 
        exponential growth function instead of a step function
        Parameters
        ----------
        x: list
            the time array that will be used to store the calculated values
        N: float
            the number of integrators in the model, in this case is not used as is fixed to 2
        t0: float
            the time of arrival of the pulse
        amp: float
            the amplitude of the pulse
        tau: float
            the exponential time of the pulse, $\tau_{i} = \tau_{d} = \tau_{r} = tau$
        offset: float
            the baseline constant value
        k: float
            the rate of amplitude of the reflected pulse
        dt: float
            the delay time of the reflection
        Returns
        -------
        crrcn: Numpy.array
            a vector containing the simulated values 
        """
        
        PLOG = 0.2784645427610738
        
        crrcn = np.zeros(np.shape(x))
        norm = (-2 * tau**2 + np.exp((2 *(tau + tau * PLOG))/ tau)*(2 * tau**2 - 4 * tau *(tau + tau * PLOG) \
            + 4*(tau + tau * PLOG)**2))/ np.exp((4 *(tau + tau * PLOG))/ tau)
        for t in x.astype(int):
            if t < t0:
                crrcn[t] = int(offset)
                
            elif t0 <= t and t < t0 + dt:
                crrcn[t] = offset + (amp * (np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + 
                    np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau))))) / norm
            elif t0 + dt <= t: 
                crrcn[t] = offset + (amp * (np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + 
                    np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau)))) + \
                    k * amp * (np.exp((-2 * t + (t0 + dt))/tau)*(-2 * np.exp((t0 + dt)/tau)* tau**2 + 
                    np.exp(t / tau)*(t**2 + (t0 + dt)**2 + 2 * (t0 + dt) * tau + 2 * tau**2 - 2 * t * ((t0 + dt) + tau)))))/norm
        return crrcn
        


    ## Compute noise sigma value based on requested SNR and expected signal amplitude
    def snrToSigma(self, snr, signalAmplitude):
        return signalAmplitude / snr
        return signalAmplitude / (10 ** (snr / 10))

    
    ## Compute expected signal amplitude based on requested SNR, while setting Noise sigma = 1
    def snrToSignalAmp(self, snr, patternPulse = False, noiseSigma = 1):
        # In case a variance-based SNR computation is required
        if type(patternPulse) != bool:
            patternStd = np.std(patternPulse)
            return noiseSigma * np.sqrt(snr) / patternStd
        
        # Otherwise, if a simple SNR = A / N computation is required
        else:
            return noiseSigma * snr


    # Generate a synthetic gaussian noise signal that includes a low-frequency (dependant)
    # component, based on an IIR approach. lowFreqModulation stands for dependency proportion
    def pmtNoise(self, noiseLength, noiseSigma = 1, lowFreqModulation = 0.2):
        seed = int(time())
        alpha = lowFreqModulation
        np.random.seed(seed)
        noiseArray = np.array([])
        currentNoise = np.random.normal(0, noiseSigma)
        for i in range(noiseLength):
            noiseArray = np.append(noiseArray, currentNoise*alpha + (1 - alpha)*np.random.normal(0, noiseSigma))
            currentNoise = noiseArray[-1]
        
        noiseArray -= np.mean(noiseArray) #Mean 0 noise
        # noiseArray = np.abs(noiseArray) #In case only low-pass filter is required
        noiseArray *= noiseSigma
        
        return noiseArray


    # Random PMT pulse generator. Creates a random trace with multiple pulses, with
    # random amplitudes and occurrence (configurable parameters)
    def pmtTrace(self, nPulses, pulseLength, pulseType, leadingProportion = 0.2,
                minAmp = 1, maxAmp = 1, traceNoiseSigma = 0, lowFreqNoiseContribution = 0.0,
                minSeparation = 0, maxSeparation = 0, returnPulseLocations = False, plotNoiseDist = False,
                quantizationBits = 0, amplitudeFromSNR = False, returnNoiselessTrace = False,
                leadingZeroes = False, trailingZeroes = False):
        trace = np.array([])
        
        if leadingZeroes:
            trace = np.append(trace, np.array([0]*pulseLength*2))
        
        pulsePositions = []
        pulseLocation = len(trace)

        self.setMaxTraceAmp(maxAmp)
            
        for i in range(nPulses):
            if returnPulseLocations:
                pulsePositions.append(pulseLocation)
            if not amplitudeFromSNR:
                thisAmp = np.random.uniform(minAmp, maxAmp)
            else:
                thisAmp = 1
            thisSeparation = int(np.random.uniform(minSeparation, maxSeparation))
            pulseLocation += thisSeparation
            thisPulse = pulseType(length = pulseLength,
                                    amplitude = thisAmp,
                                    noiseSigma = 0,
                                    leadingProportion = leadingProportion,
                                    quantizationBits = 0)
            trace = np.append(trace, thisPulse)
                    
            trace = np.append(trace, np.array([0]*thisSeparation))
        
        if trailingZeroes:
            trace = np.append(trace, np.array([0]*pulseLength*2))
            
        if amplitudeFromSNR:
            # In case of variance-based 
            # scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR), patternPulse = trace, noiseSigma = 1)
            
            # Simulating simple SNR = A / N, instead of variance-based SNR
            scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR))
            
            print("Scaling factor for SNR: " + str(scaleFactor))
            
            trace *= scaleFactor
            
        noiselessTrace = trace.copy()
        
        noiseSignal = self.pmtNoise(noiseLength = len(trace),
                                    noiseSigma = traceNoiseSigma,
                                    lowFreqModulation = lowFreqNoiseContribution)
        
        
        if plotNoiseDist:
            plt.hist(noiseSignal, bins = 100)
            plt.legend(["Noise distribution"])
            plt.show()
        
        trace += noiseSignal
        
        if quantizationBits:
            trace = self.quantize(trace, 2**quantizationBits)
        
        returnList = [trace,]
        
        if returnPulseLocations:
            returnList.append(pulsePositions)
        
        if returnNoiselessTrace:
            returnList.append(noiselessTrace)
        
        if len(returnList) == 1:
            return returnList[0]
        else:
            return returnList
        



    # Random ECAL pulse generator. Creates a random trace with multiple pulses, with
    # random amplitudes and occurrence (configurable parameters)
    def ecalTrace(self, nPulses, pulseLength, pulseType, tau = 0.2, k = 0, dt = 0, vOffset = 0,
                minAmp = 1, maxAmp = 1, traceNoiseSigma = 0, lowFreqNoiseContribution = 0.0,
                minSeparation = 0, maxSeparation = 0, returnPulseLocations = False, plotNoiseDist = False,
                quantizationBits = 0, amplitudeFromSNR = False, returnNoiselessTrace = False,
                leadingZeroes = False, trailingZeroes = False):
        trace = np.array([])
        
        if leadingZeroes:
            trace = np.append(trace, np.array([0]*pulseLength*2))
        
        pulsePositions = []
        pulseLocation = len(trace)

        self.setMaxTraceAmp(maxAmp)
            
        for i in range(nPulses):
            if returnPulseLocations:
                pulsePositions.append(pulseLocation)
            if not amplitudeFromSNR:
                thisAmp = np.random.uniform(minAmp, maxAmp)
            else:
                thisAmp = 1
            thisSeparation = int(np.random.uniform(minSeparation, maxSeparation))
            pulseLocation += thisSeparation
            
            
            
            thisPulse = pulseType(length = pulseLength,
                                    amplitude = thisAmp,
                                    tau = tau,
                                    reflectionAmplitude = k,
                                    reflectionDelay = dt,
                                    noiseSigma = 0,
                                    verticalOffset = vOffset,
                                    quantizationBits = 0)
            trace = np.append(trace, thisPulse)
                    
            trace = np.append(trace, np.array([0]*thisSeparation))
        
        if trailingZeroes:
            trace = np.append(trace, np.array([0]*pulseLength*2))
            
        if amplitudeFromSNR:
            # In case of variance-based 
            # scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR), patternPulse = trace, noiseSigma = 1)
            
            # Simulating simple SNR = A / N, instead of variance-based SNR
            scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR))
            
            print("Scaling factor for SNR: " + str(scaleFactor))
            
            trace *= scaleFactor
            
        noiselessTrace = trace.copy()
        
        noiseSignal = self.pmtNoise(noiseLength = len(trace),
                                    noiseSigma = traceNoiseSigma,
                                    lowFreqModulation = lowFreqNoiseContribution)
        
        
        if plotNoiseDist:
            plt.hist(noiseSignal, bins = 100)
            plt.legend(["Noise distribution"])
            plt.show()
        
        trace += noiseSignal
        
        if quantizationBits:
            trace = self.quantize(trace, 2**quantizationBits)
        
        returnList = [trace,]
        
        if returnPulseLocations:
            returnList.append(pulsePositions)
        
        if returnNoiselessTrace:
            returnList.append(noiselessTrace)
        
        if len(returnList) == 1:
            return returnList[0]
        else:
            return returnList
        
        
        
    # Random PMT pulse generator. Creates a random trace with multiple pulses, with
    # random amplitudes and occurrence (configurable parameters), including pileup
    def pmtTraceWithPileup(self, nPulses, pulseLength, pulseType, leadingProportion = 0.2,
                minAmp = 1, maxAmp = 1, traceNoiseSigma = 0, lowFreqNoiseContribution = 0.0,
                normalizedBeta = 1, returnPulseLocations = False, plotNoiseDist = False,
                quantizationBits = 0, amplitudeFromSNR = False, returnNoiselessTrace = False,
                leadingZeroes = False, trailingZeroes = False, samplingPeriod = 1.0e-9):
        trace = np.array([])
        
        
        
        if leadingZeroes:
            leadingZeroesCount = pulseLength*2
        else:
            leadingZeroesCount = 0
        
        individualPulses = [[] for _ in range(nPulses)]
                
        pulsePositions = []
        pulseLocation = leadingZeroesCount
        

        self.setMaxTraceAmp(maxAmp)
        
        # Determine the pulserate generation
        beta = normalizedBeta * pulseLength
            
        for pulseIndex in range(nPulses):

            if not amplitudeFromSNR:
                thisAmp = np.random.uniform(minAmp, maxAmp)
            else:
                thisAmp = 1

            # Individual pulse synthesis
            thisPulse = pulseType(length = pulseLength,
                                    amplitude = thisAmp,
                                    noiseSigma = 0,
                                    leadingProportion = leadingProportion,
                                    quantizationBits = 0,
                                    samplingPeriod = samplingPeriod)
            
            thisPulse = np.array(thisPulse)
            
            thisSeparation = int(np.random.exponential(beta))    
            
            
            if pulseIndex > 0:
                pulseLocation = len(individualPulses[pulseIndex - 1]) - pulseLength + thisSeparation
            
            # print("Pulse location zeroes: " + str([0]*pulseLocation))
            # print("This pulse: " + str(thisPulse))
            # print("Append: " + str(np.append([0]*pulseLocation, thisPulse)))
            
            if returnPulseLocations:
                pulsePositions.append(pulseLocation)
            
            thisPulse = np.append([0]*pulseLocation, thisPulse)
            
            individualPulses[pulseIndex] = thisPulse.copy()

        
        maxLen = len(individualPulses[-1])
        
        # Add remaining zeroes to each individual pulse trace
        for pulseIndex in range(nPulses - 1):                
            individualPulses[pulseIndex] = np.append(individualPulses[pulseIndex], [0]*(maxLen - len(individualPulses[pulseIndex])))
        
        trace = np.append(trace, individualPulses[0])
          
        # Sum individual pulses into a single trace
        for pulseIndex in range(1, nPulses):
            trace = trace + individualPulses[pulseIndex]
            
        
        # Add trailing zeroes if exist
        if trailingZeroes:
            trace = np.append(trace, np.array([0]*pulseLength*2))
            
        if amplitudeFromSNR:
            # In case of variance-based 
            # scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR), patternPulse = trace, noiseSigma = 1)
            
            # Simulating simple SNR = A / N, instead of variance-based SNR
            scaleFactor = self.snrToSignalAmp(snr = float(amplitudeFromSNR))
            
            print("Scaling factor for SNR: " + str(scaleFactor))
            
            trace *= scaleFactor
            
        noiselessTrace = trace.copy()
        
        noiseSignal = self.pmtNoise(noiseLength = len(trace),
                                    noiseSigma = traceNoiseSigma,
                                    lowFreqModulation = lowFreqNoiseContribution)
        
        
        if plotNoiseDist:
            plt.hist(noiseSignal, bins = 100)
            plt.legend(["Noise distribution"])
            plt.show()
        
        trace += noiseSignal
        
        if quantizationBits:
            trace = self.quantize(trace, 2**quantizationBits)
        
        returnList = [trace,]
        
        if returnPulseLocations:
            returnList.append(pulsePositions)
        
        if returnNoiselessTrace:
            returnList.append(noiselessTrace)
        
        if len(returnList) == 1:
            return returnList[0]
        else:
            return returnList

    
    def setMaxTraceAmp(self, value):
        self.__maxTraceAmp = value

    def getMaxTraceAmp(self):
        return self.__maxTraceAmp

   

'''
======================================================
Hardware emulation of pearson implementation v2 (Ivan)
======================================================
'''


class HwPearson2(object):
    
    def __init__(self):
        pass
    
    # Emulation of a sliding window average implementation in hardware
    def hwAverage(self, dataStream, windowLen):
        #Emulates the continuous data stream "infinite" length
        
        x = dataStream
        streamLen = len(x) 
        
        #Sliding window (moving average)
        avgData = []
        
        
        #This loop wouldn't exist in VHDL, since an infinite stream is expected
        for i in range(streamLen):
            
            #Be sure to not overflow the index (this won't happen in a continuous implementation, though)
            if i < streamLen - windowLen:
                #Single moving average result
                thisAvg = 0
                for j in range(i, i + windowLen):
                    thisAvg += int(x[j] / windowLen)
                
                #Append this iteration to the average list
                avgData.append(thisAvg)
            
            # Workaround for index overflow. This won't happen in a continuous real-time implementation
            else:
                thisAvg = 0
                for j in range(i, streamLen):
                    thisLen = streamLen - i
                    thisAvg += int(x[j] / thisLen)
                
                avgData.append(thisAvg)
                
        
        return avgData
    
    
    
    
    # Emulation of a absolute mean deviation implementation in hardware
    # intended to be used in a single iteration (computation) for Pearson
    def hwAMD(self, dataStream, averagedData, windowLen, averageLocation, scalingFactor = 1):
        x = dataStream
        xAvg = averagedData
  
        streamLen = len(x)
        
        # print("dataLen: " + str(streamLen))
        # print("avrgLen: " + str(len(xAvg)))
        
        
        meanDev = []
        
        #Sliding absolute mean deviation emulating continuous data flow
        for i in range(streamLen):


            thisMeanDev = 0

            # print("Window | Average: [" + str(i) + ", " + str(i + windowLen) + "]  |  " + str(i))

            # Before the sliding window reaches the trailing edge
            if i < streamLen - windowLen:                

                # "Future" average is used to improve computation accuracy of mean deviation
                for j in range(i, i + windowLen):
                    
                    # Average location must be smaller than windowLen
                    thisMeanDev += float(np.abs(x[j] - xAvg[i + averageLocation]) * scalingFactor)
                
                    # print("j: " + str(j) + "  |  avgIdx: " + str(i + averageLocation))
                
                
            
            else:
                        
                for j in range(i, streamLen):
                    thisLen = streamLen - i
                    if thisLen <= averageLocation:
                        thisMeanDev += float(np.abs(x[j] - xAvg[i + thisLen - 1]) * scalingFactor)
                    else:
                        # print("len: " + str(thisLen) + "  |  j: " + str(j) + "  |  avgIdx: " + str(i + averageLocation))
                        thisMeanDev += float(np.abs(x[j] - xAvg[i + averageLocation]) * scalingFactor)
                
                
            #Append this iteration to the final list
            meanDev.append(thisMeanDev / windowLen)
                

                
        return meanDev
    
    
    
    
    
    # "Continuous" pearson correlation implemented in a hardware-emulated fashion
    def hwPearson(self, x, y, windowLen, averageLocation):
        
        #Both lists must be equally sized
        if len(x) != len(y):
            return None
        
        
        dataLen = len(x)
                    
        
        # Average location must be smaller than windowLen
        if averageLocation >= windowLen:
            averageLocation = windowLen - 1
            
            
        #The following computations emulate a parallel implementation of mean and amd in hardware
        
        #Computing average arrays for each data stream
        xM = self.hwAverage(x, windowLen)
        yM = self.hwAverage(y, windowLen)
        
        #Computing absolute mean deviations (AMD) for each data stream
        xD = self.hwAMD(x, xM, windowLen, averageLocation)
        yD = self.hwAMD(y, yM, windowLen, averageLocation)
        
              
        # print("len xD: " + str(len(xD)))
        # print("len yD: " + str(len(yD)))
        
        
        # Resulting list with correlation values while sliding
        # through parallel data stream windows
        correlation = 0
        
        # print("DataLen: " + str(dataLen))
        # print("WndwLen: " + str(windowLen))
        # print("AvrgLoc: " + str(averageLocation))
        
        
        
        #Emulating an infinite stream of continuous data flow
        thisCorrelation = 0
        for i in range(dataLen):
            
            if i < dataLen - averageLocation:
                
                if(float(xD[i]) == float(0)):
                    xD[i] = 0.0000001
                
                if(float(yD[i]) == float(0)):
                    yD[i] = 0.0000001
                
                #This iteration's x's standard score                
                xScore = ((x[i] - xM[i + averageLocation]) / (xD[i]))
                
                
                #The same for y
                yScore = ((y[i] - yM[i + averageLocation]) / (yD[i]))
                

                #Correlation computation for this iteration
                thisCorrelation = xScore * yScore * dataLen
                
                # print(thisCorrelation)
                
                
                
                correlation += thisCorrelation
            
            else:
                
                thisLen = (dataLen - averageLocation) - i
                
                thisMeanX = x[i] - xM[dataLen - 1]
                thisMeanY = y[i] - yM[dataLen - 1]
                
                xScore = ((thisMeanX) / thisLen)
                yScore = ((thisMeanY) / thisLen)
                
                thisCorrelation = xScore * yScore * thisLen
                
                correlation += thisCorrelation
                
            # print("{Corr:.4f} @ [{index}]".format(Corr = thisCorrelation, index = i))
                    
        return correlation



    # Simplified version of Pearson. Only for correlation, not cross correlation.
    # Expects:
    #   x -> emulation of continuous signal
    #   y -> static signal (to extract coefficients from)
    #
    # Returns:
    #   <list> <- correlation between dynamic (x) and coefficients (y) on sliding window

    def hwPearson2(self, x, y, scalingFactor = 1):
                
        c_i = self.__getCoefficients(y) # Normalized coefficients

        # Sliding window length must match that from y's length
        windowLen = len(y)

        # Length of continuous data stream
        dataLen = len(x)

        # Higher correlation of AMD with STD if average is computed in the middle
        avgLocation = windowLen // 2

        correlation = []


        print("Data length: " + str(dataLen))

         # Signal average (x Mean)
        xM = np.array(self.hwAverage(x, windowLen))
        print("Averages done!")

        # Signal average mean deviation (x Deviation)
        xD = np.array(self.hwAMD(x, xM, windowLen, avgLocation))
        print("Absolute mean deviations done!")
        
        
        x2 = np.array(x)

        
        corr = 0
        
        for i in range(windowLen):
            corr += ((x2[i] - xM[avgLocation]) / xD[avgLocation]) * c_i[i]
        
        return corr
        
        

        for i in range(dataLen - windowLen):
           
            # Computing correlation in sliding window
            thisR = 0
            print("Iteration: " + str(i + 1) + " / " + str(dataLen))

            for j in range(i, i + windowLen):
                
                # x_j - x_avg
                r = (x[j] - xM[i + avgLocation]) * int(scalingFactor)
                
                # divided by absolute mean deviation
                r //= xD[i + avgLocation]
                
                # product with FIR coefficients
                r *= c_i[j % windowLen] #Coefficients are shorter than continuous signal
                
                thisR += r

            correlation.append(thisR)
        
        for i in range(dataLen - windowLen, dataLen):
            print("Iteration: " + str(i + 1) + " / " + str(dataLen))
            correlation.append(0)

        # Intended to reduce rounding error of normalized integer correlation
        correlation = np.array(correlation)
        
        
        #Normalization
        correlation /= windowLen         

        return correlation
        



    # Compute static coefficients of reference signal (to correlate with)
    def __getCoefficients(self, y):
        meanY = np.average(y)
        stdY  = np.std(y)

        
        y = np.array(y).astype(float)
        coeff = y - meanY
        coeff /= stdY
        
        coeff = coeff / np.dot(coeff, coeff)
        
        
        plt.plot(coeff)
        plt.legend(["Pattern signal"])
        plt.ylabel("Amplitude")
        plt.xlabel("Samples")
        plt.show()

        return coeff


        
        
            
            
            
            
        


        






'''
============================================
Hardware emulation of pearson implementation
============================================
'''

class HwPearson(object):
    def __init__(self):
        pass

        
    # Hardware emulation of average computation
    def hwMean(self, data, windowSize):
        n = windowSize
        xm2 = []
        a = 0
        b = 0
        c = 0

        for i in range(len(data)):
            xm2.append((int(b / n)))
            a = data[i]
            if i<n:
                c = 0
            else:
                c = data[i-n]
            b = a + b - c
        return xm2


    # Hardware emulation of static mean deviation computation
    def meanDevStatic(self, data, windowSize):

        m = windowSize

        MDs=[]
        for i in range(len(data)):
            window = data[i:i+m]
            wAvrg = statistics.mean(window)
            tot = 0
            for d in window:
                tot += abs(d - wAvrg)
            MDs.append(int(tot / m))

        return MDs


    # Hardware emulation of dynamic (real-time) mean deviation computation
    def hwMeanDev(self, data, averagedData, shiftRegisterSize):
        
        n = shiftRegisterSize 
        m = int(n / 2)
        
        srt = range(n) #Shift register index

        for d in srt:
            sr = d ##Shift register for pulse_data
            adIf = []  ###|x-xm|
            MDd = []

            #This should emulate the continuous data flow (just send data long enough)
            for i in range(len(data)): 
                if i<sr: #Nothing to do if not enough data yet
                    adIf.append(0)
                else: # |x - xm| <-- absolute mean average respect to moving average window
                    adIf.append(abs(data[i-sr] - averagedData[i]))

            a=0
            b=0
            c=0

            for i in range(len(data)):
                MDd.append((int(b / m)))
                a = adIf[i]
                if i<m:
                    c = 0
                else:
                    c = adIf[i - m]
                b = a + b - c

        return MDd



    # Emulation of pearson in hardware implementation
    def hwPearson(self, dataX, dataY, windowLength):
        m = windowLength
        n = 2 * m #Shift register length

        x = dataX
        y = dataY


        #Let's ensure that both arrays have the same length
        if not len(x) == len(y):
            return None
        
        dataLen = len(x)

        #Sliding window average
        #Pre-computed results (imagine a parallel FPGA impelmentation)
        meanX = self.hwMean(x, m)
        meanY = self.hwMean(y, m)


        #Sliding window absolute mean deviation
        #Pre-computed results (imagine a parallel FPGA impelmentation)
        mdX = self.hwMeanDev(x, meanX, n)
        mdY = self.hwMeanDev(y, meanX, n)

        #Returning modified pearson coefficient        
        thisPearson = 0

        #Computing modified pearson coefficient
        for i in range(dataLen):
            currentXMean = (x[i] - meanX[i])
            currentYMean = (y[i] - meanY[i])

            currentXmD = (mdX[i])
            currentYmD = (mdY[i])
            
            if currentXmD == 0:
                currentXmD = 1
            
            if currentYmD == 0:
                currentYmD = 1

            #Sum of the products of standard scores
            thisPearson += ((currentXMean / currentXmD)) * ((currentYMean / currentYmD))

        return thisPearson * 2 / dataLen**2



if __name__ == '__main__':

    TEST_PULSE_LEN = 64
    MIN_PULSE_AMPLITUDE = 10
    MAX_PULSE_AMPLITUDE = 135
    NOISE_AMPLITUDE = 20
    ORIGINAL_PULSE_REPLICAS = 10
    LEADING_TO_LENGTH_RATIO = 0.45
    DISTANCE_BETWEEN_PULSES = [TEST_PULSE_LEN * 1 , TEST_PULSE_LEN * 5]
    LOW_FREQ_NOISE_INDEX = 0.25


    AVERAGING_WINDOW_SIZE = TEST_PULSE_LEN

    #Visual line value to show "valid" correlation threshold
    CORRELATION_THRESHOLD_LINE = 0.75




    
        
    pC = PearsonComparison(0) 
    hP = HwPearson()
    hp2 = HwPearson2()
    signal = PulseSynthesis()






    # Ideal PMT pulse
    x = signal.pmtPulse(length = TEST_PULSE_LEN,
                amplitude = MIN_PULSE_AMPLITUDE,
                noiseSigma = 0, #Ideal pulse means NO NOISE
                leadingProportion = LEADING_TO_LENGTH_RATIO)

    # #Ideal square pulse
    # x = signal.squaredPulse(length = TEST_PULSE_LEN,
    #             amplitude = MIN_PULSE_AMPLITUDE,
    #             noiseSigma = 0,
    #             leadingProportion = 0.5
    #             )


    plt.plot(np.arange(len(x)), x, 'r-')
    plt.legend(["Expected signal"])
    plt.show()


    # USE THIS DEFINITION TO EMULATE NOISY PMT PULSES
    y = signal.pmtTrace(nPulses = ORIGINAL_PULSE_REPLICAS,
                pulseLength = TEST_PULSE_LEN,
                pulseType = signal.pmtPulse,
                leadingProportion = LEADING_TO_LENGTH_RATIO,
                minAmp = MIN_PULSE_AMPLITUDE,
                maxAmp = MAX_PULSE_AMPLITUDE,
                traceNoiseSigma = NOISE_AMPLITUDE,
                lowFreqNoiseContribution = LOW_FREQ_NOISE_INDEX,
                minSeparation = min(DISTANCE_BETWEEN_PULSES), 
                maxSeparation = max(DISTANCE_BETWEEN_PULSES),
                )
    
    # y2 = signal.pmtTraceWithPileup(nPulses = ORIGINAL_PULSE_REPLICAS,
    #                                pulseLength = TEST_PULSE_LEN,
    #                                pulseType = signal.pmtPulse,
    #                                leadingProportion = LEADING_TO_LENGTH_RATIO,
    #                                minAmp = MIN_PULSE_AMPLITUDE,
    #                                maxAmp = MAX_PULSE_AMPLITUDE,
    #                                traceNoiseSigma = NOISE_AMPLITUDE,
    #                                lowFreqNoiseContribution = LOW_FREQ_NOISE_INDEX,
    #                                normalizedBeta = 0.5
    #                                )
    
    y2, y2Pos = signal.pmtTraceWithPileup(nPulses = 100,
                                   pulseLength = TEST_PULSE_LEN,
                                   pulseType = signal.pmtPulse,
                                   leadingProportion = LEADING_TO_LENGTH_RATIO,
                                   minAmp = 1,
                                   maxAmp = 1,
                                   traceNoiseSigma = 1,
                                   amplitudeFromSNR = 10,
                                   lowFreqNoiseContribution = LOW_FREQ_NOISE_INDEX,
                                   leadingZeroes = True,
                                   trailingZeroes = True,
                                   returnPulseLocations = True,
                                   normalizedBeta = 5
                                   )

    print("Pulse positions with pile-up: "+ str(y2Pos))
    
    # USE THIS DEFINITION TO EMULATE NOISY SQUARED PULSES
    # y = signal.pmtTrace(nPulses = ORIGINAL_PULSE_REPLICAS,
    #             pulseLength = TEST_PULSE_LEN,
    #             pulseType = signal.squaredPulse,
    #             leadingProportion = 0.5,
    #             minAmp = MIN_PULSE_AMPLITUDE,
    #             maxAmp = MAX_PULSE_AMPLITUDE,
    #             tracenoiseSigma = NOISE_AMPLITUDE,
    #             lowFreqNoiseContribution = LOW_FREQ_NOISE_INDEX,
    #             minSeparation = min(DISTANCE_BETWEEN_PULSES), 
    #             maxSeparation = max(DISTANCE_BETWEEN_PULSES),
    #             )


    # USE THIS DEFINITION TO EMULATE NOISE-LESS PMT PULSES
    # y = signal.pmtTrace(nPulses = ORIGINAL_PULSE_REPLICAS,
    #              pulseLength = TEST_PULSE_LEN,
    #              pulseType = signal.pmtPulse,
    #              leadingProportion = LEADING_TO_LENGTH_RATIO,
    #              minAmp = 1,
    #              maxAmp = 1,
    #              tracenoiseSigma = 0.0,
    #              minSeparation = min(DISTANCE_BETWEEN_PULSES), 
    #              maxSeparation = max(DISTANCE_BETWEEN_PULSES),
    #              )




    # a = hp2.hwPearson2(y, x)

    # print("Lengths [x, correlation] = [{lX:d}, {lC:d}]".format(lX = len(y), lC = len(a)))

    
    # plt.plot(y, label = "Signal")
    # plt.plot(a, label = "Correlation")
    # plt.legend()
    # plt.show()


    print("==============================")
    
    print(hp2.hwPearson2(x, x))
    print(pC.pearsonScipy(x, x))
    
    print("==============================")


    # import dcor
    correlations = [[], [], [], []] #Scipy , Modified Pearson, Distance correlation, Hardware-emulated Pearson


    # for i in range(len(x)):
    # for i in range(int(len(x)/2), int(len(x)*3/2)):
    # for i in range(0, int(len(x)*2)):
    for i in range(len(y) - len(x)):
        yS = np.array(y[i:i+len(x)])   
        
        correlations[0].append(pC.pearsonScipy(x, yS))
        correlations[1].append(pC.pearsonModified(x, yS))
        # correlations[2].append(dcor.distance_correlation(x, yS))

        #Added hardware emulation of pearson implementation. To be tested yet...
        # correlations[3].append(hP.hwPearson(x, yS, 32))
        
        # Added hardware emulation using new implementation (Ivan)
        # correlations[3].append(hp2.hwPearson(x, yS, AVERAGING_WINDOW_SIZE, AVERAGING_WINDOW_SIZE // 2))

    
    for i in range(len(y) - len(x), len(y)):
        correlations[0].append(0)
        correlations[1].append(0)
        # correlations[2].append(0)
    
    # correlations[3] = hp2.hwPearson2(y, x, scalingFactor = 1)
        




    plt.figure(1)
    plt.subplot(211)
    plt.plot(y, '-', linewidth = 0.8, markeredgewidth = 0.1)
    plt.legend(["Synthetic signal"])
    plt.ylabel("Amplitude")
    plt.xlabel("Samples")



    plt.subplot(212)
    xAxis = np.arange(len(correlations[0]))
    plt.plot(xAxis, [CORRELATION_THRESHOLD_LINE]*len(xAxis), 'g--', linewidth = 0.75)
    
    # Comparison between classic correlation and modified corelation
    plt.plot(xAxis, correlations[0], correlations[1], linewidth = 0.8)
    plt.legend(["Correlation threshold", "Classic correlation", "Modified correlation"])

    # # Comparison between classic correlation and distance correlation
    # plt.plot(xAxis, correlations[0], correlations[2], linewidth = 0.8)
    # plt.legend(["Correlation threshold", "Classic correlation", "Distance correlation"])


    # Comparison between classic correlation and hardware-emulated correlation
    # plt.plot(xAxis, correlations[0], '-')
    # plt.plot(correlations[3], '-')
    # plt.legend(["Correlation threshold", "Classic correlation", "Hardware correlation"])



    plt.xlabel("Signal shift (samples)")
    plt.ylabel("Correlation")
    plt.show()



    plt.plot(y2, '-', linewidth = 0.8, markeredgewidth = 0.1)
    plt.xlabel("Samples")
    plt.ylabel("Input signal amplitude")
    plt.title("Input signal with pile-up")
    plt.show()




    # Some performance tests
    '''

    N_RUNS = 10000

    print("Result SciPy:     " + str(pC.pearsonScipy(x,y)))
    print("Result Normal:    " + str(pC.pearsonBareMetal(x,y)))
    print("Result Optimized: " + str(pC.pearsonModified(x,y)))




    pC.benchmarkScipy(x, y, N_RUNS)
    pC.benchmarkBareMetal(x, y, N_RUNS)
    pC.benchmarkOptimized(x, y, N_RUNS)

    '''
