'''
Pulse synthesis library to emulate nuclear detector events
'''

import numpy as np
import matplotlib.pyplot as plt
from pearsonComparison import PulseSynthesis

class NuclearTrace(object):
    
 
    
    def __init__(self,
                 pulseLen : np.int, 
                 pulseMinAmp : np.float,
                 pulseMaxAmp : np.float,
                 noiseSigma : np.float,
                 pulseCount : np.int,
                 pulseSkew : np.float,
                 normalizedBeta : np.float = 10,
                 addLeadingZeroes : bool = True,
                 addTrailingZeroes : bool = True
                 ):
        '''
        !
        !@brief  Initializing pulse synthesizer with emulation parameters
        !@param  pulseLen: Number of samples for each pulse
        !@param  pulseMinAmp: Minimum pulse amplitude in the trace (random value)
        !@param  pulseMaxAmp: Maximum pulse amplitude in the trace (random value)
        !@param  noiseSigma: Additive white Gaussian noise sigma in the trace
        !@param  pulseCount: Total number of events to be synthesized in the trace
        !@param  pulseSkew: Skewness of each pulse shape
        !@param  normalizedBeta: Pulses are distributed as a Poisson process (beta = 1 - lambda)
        !@param  addLeadingZeroes: Add zero values before the first pulse appearance
        !@param  addTrailingZeroes: Add zero values after the last pulse appearance
        '''
       
        self.pulseLen = pulseLen
        self.pulseMinAmp = pulseMinAmp
        self.pulseMaxAmp = pulseMaxAmp
        self.noiseSigma = noiseSigma
        self.pulseCount = pulseCount
        self.pulseSkew = pulseSkew
        self.normalizedBeta = normalizedBeta
        self.addLeadingZeroes = addLeadingZeroes
        self.addTrailingZeroes = addTrailingZeroes
        
        # Initialize pulse synthesis library
        self.pulseSynthesis = PulseSynthesis()
    
    
    '''
    Truncate values to simulate detector saturation
    '''
    def truncateForSaturation(self, trace, maxVal):
        for i in range(len(trace)):
            if trace[i] > maxVal:
                trace[i] = maxVal        
        return trace
    
    def computeTrace(self):
        signal = self.pulseSynthesis
        
        y = signal.pmtTraceWithPileup(
            nPulses = self.pulseCount,
            pulseLength = self.pulseLen,
            pulseType = signal.pmtPulse,
            leadingProportion = self.pulseSkew,
            minAmp = self.pulseMinAmp,
            maxAmp = self.pulseMaxAmp,
            traceNoiseSigma = self.noiseSigma,
            leadingZeroes = self.addLeadingZeroes,
            trailingZeroes = self.addTrailingZeroes,
            normalizedBeta = self.normalizedBeta
        )
    
        signalLen = len(y)
        x = np.linspace(0, signalLen - 1, signalLen)
               
        return (x,y)
    
    def mirrorTraceY(self, trace : np.array, baseline : np.float = 0.0, intOutput = True):
        trace *= -1
        trace += baseline
        if intOutput:
            trace = trace.astype(np.int)
        return trace
    
    def plotTrace(self, x, y):
        plt.plot(x,y)
        plt.show()
        
    def createHeaderFileC(self, trace, filename):
        f = open(filename, 'w')
        f.write("#ifndef PMT_TRACE_H\n")
        f.write("#define PMT_TRACE_H\n")
        
        f.write("const unsigned int PMT_TRACE[] = {\n")
        for i in range(len(trace) - 1):
            f.write(f"{trace[i]},\n")
        
        f.write(str(trace[len(trace) - 1]))
        f.write("\n};\n")      
        f.write("#endif")
        
        f.close()
        
    
        
    
if __name__ == '__main__':
    
    PULSE_COUNT = 100
    PULSE_LENGTH = 64
    PULSE_AMP_MIN = 64
    PULSE_AMP_MAX = 10000
    PULSE_SKEW_FACTOR = 0.25
    NOISE_SIGMA = 10
    PULSE_SEPARATION_BETA = 20
    BASELINE = 8192
    SIMULATE_SATURATION = True
    REVERSE_PULSE = True
    OUTPUT_FILE = './Labs/LabProject/src/pmt_trace.h'
    
    nuclearPulses = NuclearTrace(
        pulseLen = PULSE_LENGTH,
        pulseMinAmp = PULSE_AMP_MIN,
        pulseMaxAmp = PULSE_AMP_MAX,
        noiseSigma = NOISE_SIGMA,
        pulseCount = PULSE_COUNT,
        pulseSkew = PULSE_SKEW_FACTOR,
        normalizedBeta = PULSE_SEPARATION_BETA
    )
    
    x,y = nuclearPulses.computeTrace()
    nuclearPulses.plotTrace(x,y)
    yFinal = y
    
    if SIMULATE_SATURATION:
        yFinal = nuclearPulses.truncateForSaturation(yFinal, BASELINE)
    
    if REVERSE_PULSE:
        yFinal = nuclearPulses.mirrorTraceY(yFinal, baseline = BASELINE)
        nuclearPulses.plotTrace(x, yFinal)
        
    if OUTPUT_FILE != False:
        nuclearPulses.createHeaderFileC(yFinal, OUTPUT_FILE)
    
    