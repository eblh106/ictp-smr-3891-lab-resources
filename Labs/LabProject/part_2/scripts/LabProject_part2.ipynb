{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab Project 1\n",
    "# Part 2: Digital Pulse Processing - isotope identification"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "A digital pulse processing (DPP) system will be developed from scratch, aiming at analyzing events recorded from a $\\gamma$ radiation detector placed close to a $\\gamma$ source.\n",
    "\n",
    "The heavy processing part is carried out in the SoC PL, where the oscilloscope, pulse-shaping, peak-detection, and histogram computations are implemented. Moreover, the control of these processing block parameters are set using the **output registers** of the **ComBlock**, which also resides in the PL. The histogram (used to obtain the spectrum of the events) stores its results in the **ComBlock's** **True DP-RAM**, while the raw **oscilloscope** traces are streamed to the **Comblock's** **Input FIFO**.\n",
    "\n",
    "The Zynq SoC PS commands the communication between the SoC PL and the computer using the **UDMA** library. \n",
    "\n",
    "This Jupyter Notebook is the interface between the SoC and the data visualization. You will interact with the SoC through UDMA to do the following:\n",
    "\n",
    "    - Set the parameters for the pulse shaper using the ComBlock output registers\n",
    "    - Set the parameters for the peak detector using the ComBlock output registers\n",
    "    - Set the parameters for the oscilloscope using the ComBlock output registers\n",
    "    - Acquire the computed histogram (spectrum) using the ComBlock true dual-port RAM\n",
    "    - Acquire the raw pulse shapes from the oscilloscope using the ComBlock input FIFO\n",
    "\n",
    "You will be provided with a library (DppParameters) to compute the 32-bit unsigned representation of the parameters to be written to the pulse shaper and the peak detector. \n",
    "\n",
    "\n",
    "> 🔴 **Before you continue**\n",
    "> Make sure you have succesfully obtained the unique detector parameters from the previous project stage: `tauDecay` ($\\tau_D$) and `tauRise` ($\\tau_R$)\n",
    "\n",
    "\n",
    "\n",
    "## Goals\n",
    "    - Compute the DPP parameters using the detector paramters 'tauDecay' and 'tauRise'\n",
    "    - Record and visualize the raw traces from the oscilloscope running in the SoC PL.\n",
    "    - Fine tune and visualize the spectrum of the signal processed by the DPP.\n",
    "    - Identify the radiation source located close to the detector, by comparing it with a known spectra database\n",
    "    - (Optional) Calibrate the detector to energy scale (in keV) using the known energy peaks of the spectrum\n",
    "\n",
    "## Procedure\n",
    "\n",
    "- A) Import the libraries and setup the global working paramters (constants)\n",
    "- B) Compute the parameters of the DPP system (pulse shaper and peak detector), using your detector's decay and rise time constants\n",
    "    - You can use the multiplexer (MUX) within the PL to change the signal shown in the external oscilloscope\n",
    "        - Raw input signal\n",
    "        - Pulse shaper outputs\n",
    "        - Peak detector output\n",
    "- C) Connect to the SoC via UDMA and send the parameters for:\n",
    "    - Pulse shaper\n",
    "    - Peak detector\n",
    "    - Oscilloscope\n",
    "- D) Acquire multiple pulses from the SoC PL using the UDMA.\n",
    "    - You may use the ```recordDataset``` method from the ```PulseFit``` library, if you prefer.\n",
    "    - Change the oscilloscope parameters in the ComBlock to achieve the best pulse acquisition performance\n",
    "        - You may change the ```Threshold``` and ```Samples_before_trigger``` (delay_max = 1023 samples). \n",
    "        - Avoid changing the ```Pulse Length``` value\n",
    "- E) Acquire the spectrum computed in the ```Histogram``` block of the DPP. \n",
    "    - Clean the DP RAM contents \n",
    "    - Read the ComBlock RAM contents using the UDMA library to acquire the spectrum\n",
    "        - Clean the ComBlock RAM contents once, to make sure no previous data is left\n",
    "        - Plot the ComBlock RAM contents with the ```plt.plot``` method (do not use the Python built-in histogram function)\n",
    "        - You may execute the acquisiton->plot process multiple times to see how the spectrum starts representing a known pattern\n",
    "- F) Determine the isotope of the radiactive source that your system has detected\n",
    "    - Visually inspect the shape of the spectrum and compare it with a set of known spectra of different isotopes\n",
    "- G) (OPTIONAL) Calibrate the detector output into energy range (keV) using two (or more) points:\n",
    "    - Photopeak and baseline may be enough to achieve a rough calibration curve.\n",
    " \n",
    "Remember, the ```PulseFit.recordDataset``` method was used in the previous stage of this project\n",
    "\n",
    "---\n",
    "\n",
    "- Author(s): Ivan Morales, Romina Molina (MLab/ICTP) - 2023/11/23 (version 1.0)\n",
    "- Update(s):\n",
    "\n",
    "--- "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A) Before starting\n",
    "Importing libraries and initializing the detector parameters. Do not change any value if you are not sure of what they mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import udma\n",
    "import numpy as np\n",
    "import matplotlib.animation\n",
    "import matplotlib.pyplot as plt\n",
    "from pulsefit import PulseFit\n",
    "from dppparameters import DppParameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do not change the following parameters, unless you know the implications"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Total trace length (samples). This value must match the Vivado Block Design settings.\n",
    "TRACE_LEN = 2048     # Number of samples to be recorded (must match with the ComBlock I-FIFO length)\n",
    "SAMPLING_RATE = 50e6 # Sampling rate (fs) of the ADC (50 Msps)\n",
    "ADC_RESOLUTION = 14  # Resolution of the ADC\n",
    "DC_OFFSET = (2**ADC_RESOLUTION // 2) # Pulses are located at half of the amplitude"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### B) DPP parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Once you are satisfied with the fitting results of the pulse shape from the previous project stage, you may proceed to compute the digital pulse processor (DPP) parameters that will be executed in the PL.\n",
    "\n",
    "The two DPP components that require fine-tuned parameters in the next Laboratory session are:\n",
    "- Pulse shaper: it transforms the incoming pulses into a trapezoid with amplitude proportional to the incoming energy of the detected event.\n",
    "- Peak detector: finds individual pulse events, fed from the pulse shaper output.\n",
    "\n",
    "Execute the following commands to translate the fitted parameters into a 32-bit unsigned representation that will be deployed in the FPGA processing blocks in the next laboratory session."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Set the following detector constants with the values you obtained in the previous stage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "TAU_DECAY = ????e-6\n",
    "TAU_RISE  = ????e-6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initializing the DppParameters library with the ADC sampling rate\n",
    "dppParams = DppParameters(samplingRate = SAMPLING_RATE) # ADC sampling rate (50 Msps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute the DPP Pulse Shaper parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dppParams.computeParamsShaper(\n",
    "    tauD = TAU_DECAY,\n",
    "    tauR = TAU_RISE\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute the DPP Peak Detector parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dppParams.computeParamsPeakD()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> 🔴 **Before you continue**\n",
    "\n",
    "The ```r4_en_pkd``` register of the *Peak Detector* IP core enables/disables its functionality. This feature is useful to execute the histogram computation only during the measurement, reducing the influence of the background radiation.\n",
    "\n",
    "You have mapped this register to one of the ComBlock output registers and due to its functionality, it is not provided as a parameter in the ```DppParameters``` library.\n",
    "\n",
    "Enabling the *Peak detector* is done by setting ```r4_en_pkd = 4```. To stop the histogram computation, simply reset this register."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### C) Interface with SoC via UDMA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Connect to the SoC using UDMA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Remember to disable UDMA logging, to speed up the acquiring process"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Set the ComBlock output register values using the DPP parameters obtained in section **(B) DPP parameters**\n",
    "\n",
    "> Do not forget to add the `DC_OFFSET` constant to the oscilloscope threshold value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shaper parameters\n",
    "r1_shaper_b10_32_0 = ????\n",
    "r2_shaper_na_32_0 = ????\n",
    "r3_shaper_nb_32_0 = ????\n",
    "...\n",
    "...\n",
    "\n",
    "\n",
    "# Peak detector parameters\n",
    "r1_peakD_x_delay_32_0 = ???\n",
    "r2_peakD_x_noise_32_0 = ???\n",
    "r3_peakD_x_min_max_32_0 = ???\n",
    "\n",
    "# Oscilloscope trigger parameters\n",
    "r1_osc_threshold = DC_OFFSET + ???\n",
    "r2_osc_samplesBeforeTrigger = ???\n",
    "r3_osc_pulseLen = TRACE_LEN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Write DPP and oscilloscope parameters using ComBlock output registers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Choose the output of the DAC Mux, according to the mapping you did in Vivado"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### D) Oscilloscope"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You may increase or decrease this value\n",
    "PULSES_TO_RECORD = 50"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> You may use the `recordDatset` method from the `PulseFit` library to record the dataset of pulses\n",
    "\n",
    "🔴 Store the recorded pulses in a variable called `dataset` 🔴"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Animated plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You might not need to change these values \n",
    "PLOT_Y_AXIS_LIMITS = (8000, 16500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting your recorded dataset in oscilloscope mode"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataFrameToPlot = dataset # Assign here the name of the variable for the recorded pulses (default: dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# General configuration for VSCode\n",
    "plt.rcParams[\"animation.html\"] = \"jshtml\"\n",
    "plt.rcParams['figure.dpi'] = 125\n",
    "plt.rcParams['figure.figsize']=(9, 4)\n",
    "# Turn off interactive mode\n",
    "plt.ioff()\n",
    "fig = plt.figure()\n",
    "\n",
    "def animatePlot(t):\n",
    "    plt.cla()\n",
    "    plt.plot(dataFrameToPlot.iloc[t].T.values, color='indigo')   # <- Data\n",
    "    plt.xlabel(\"Time (samples)\")\n",
    "    plt.ylabel(\"Amplitude (ADC units)\")\n",
    "    plt.title(\"Oscilloscope\", color='indigo')\n",
    "    plt.ylim(PLOT_Y_AXIS_LIMITS[0], PLOT_Y_AXIS_LIMITS[1])\n",
    "    plt.grid()\n",
    "\n",
    "matplotlib.animation.FuncAnimation(fig, animatePlot, frames=PULSES_TO_RECORD) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### E) Spectrum from DP-RAM"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Enable peak detector IP core.\n",
    "\n",
    "🔴 Remember to set the corresponding register `4` to enable and reset to `0` to disable it 🔴"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Clean the RAM contents before proceeding. You may use the ```write_ram``` _UDMA_ command with an array full of zeros (```np.zeros```).\n",
    "\n",
    "This operation will ensure that the accumulated spectrum represents only the latest measurements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Read the contents of the ComBlock RAM\n",
    "\n",
    "🔴 Store the RAM contents in a variable called `spectrum` 🔴\n",
    "\n",
    "We are only interested in the RAM contents, not the metadata related to the number of read values. The variable must be a one-dimension list with length = 2048"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the contents of the RAM using a ```plt.plot```.\n",
    "\n",
    " We are not using a ```plt.hist```, since the histogram has already been computed in the FPGA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spectrum = np.array(spectrum).astype(np.uint32)\n",
    "\n",
    "xAxis = np.linspace(0, TRACE_LEN - 1, TRACE_LEN)\n",
    "\n",
    "plt.figure()\n",
    "plt.title(\"Spectrum from detector\")\n",
    "plt.plot(xAxis, spectrum, '-')\n",
    "plt.xlabel(\"Energy (arbitrary units)\")\n",
    "# plt.yscale('log')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### F) Determine the isotope"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll find next the spectra of several known isotopes, recorded using a sodium iodide (NaI) detector. Try to focus on the histogram features, such as the peak locations and amplitude relations.\n",
    "\n",
    "Use these features to compare the spectrum you have just obtained with the provided templates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](../img/spectra.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### G) Challenge - Energy calibration and detector resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you didn't use a **Cs-137** source, acquire the spectrum again during a couple of minutes for the next steps. \n",
    "\n",
    "Using the recorded spectrum data from the ComBlock DP-RAM, find at least one relevant peak in your spectrum and fit it to a Gaussian model to obtain the following parameters:\n",
    "- Mean ($\\mu_0$)\n",
    "- Standard deviation ($\\sigma$)\n",
    "\n",
    "You may use (for example) the ```curve_fit``` method from the ```scipy.optimize``` library.\n",
    "\n",
    "\n",
    "#### Detector resolution\n",
    "Use the standard deviation of the photopeak ($\\sigma$) to compute the full-width at half-maximum (FWHM).\n",
    "\n",
    "$$FWHM = 2*\\sigma \\sqrt{2ln2} \\approx 2.355\\sigma$$\n",
    "\n",
    "The detector resolution ($\\Delta E/E$) is typically defined as the Cs-137 phototpeak FWHM, divided by the photopeak energy locaion ($\\mu_0$).\n",
    "\n",
    "$$\\frac{\\Delta E}{E} = \\frac{FWHM}{\\mu_0}$$\n",
    "\n",
    "As a reference, the energy resolution for a typical **NaI(Tl)** detector should be arround **6%**.\n",
    "\n",
    "#### Energy calibration\n",
    "\n",
    "A curve of the form _Energy_ (y-axis) vs. _ADC value_ (x-axis) should be obtained.\n",
    "\n",
    "Since you are using Cs-137 as the calibration source, the **photopeak energy** is located at **661.6 keV** (kilo electron-Volts)\n",
    "\n",
    "Two calibration points are obtained as follows:\n",
    "- The mean value of the Gaussian fit $\\mu_0$ will represent the x-value of one point in the curve, with the Cs-137 photopeak position representing the y-value of this same point. \n",
    "- The other point represents the baseline (0, 0).\n",
    "\n",
    "You should obtain a function of the form $y = a*x + b$, with:\n",
    "- $b = 0$ (baseline)\n",
    "- $y$: energy (in kiloelectron-Volts or keV)\n",
    "- $x$: energy (in ADC units, as recorded from the detector)\n",
    "- $a$: slope of the curve (obtained from the two calibration points)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
