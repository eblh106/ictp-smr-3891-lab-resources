# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set BRAM_ADDRESS_SIZE [ipgui::add_param $IPINST -name "BRAM_ADDRESS_SIZE" -parent ${Page_0}]
  set_property tooltip {Bram Address Size in bits} ${BRAM_ADDRESS_SIZE}


}

proc update_PARAM_VALUE.BRAM_ADDRESS_SIZE { PARAM_VALUE.BRAM_ADDRESS_SIZE } {
	# Procedure called to update BRAM_ADDRESS_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BRAM_ADDRESS_SIZE { PARAM_VALUE.BRAM_ADDRESS_SIZE } {
	# Procedure called to validate BRAM_ADDRESS_SIZE
	return true
}


proc update_MODELPARAM_VALUE.BRAM_ADDRESS_SIZE { MODELPARAM_VALUE.BRAM_ADDRESS_SIZE PARAM_VALUE.BRAM_ADDRESS_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BRAM_ADDRESS_SIZE}] ${MODELPARAM_VALUE.BRAM_ADDRESS_SIZE}
}

