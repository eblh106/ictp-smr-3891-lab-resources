-- Generated from Simulink block blr/axi_clk_domain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_axi_clk_domain is
  port (
    r1_thr_lo : in std_logic_vector( 32-1 downto 0 );
    r2_thr_hi : in std_logic_vector( 32-1 downto 0 );
    r3_m : in std_logic_vector( 32-1 downto 0 );
    r4_reset : in std_logic_vector( 32-1 downto 0 )
  );
end blr_axi_clk_domain;
architecture structural of blr_axi_clk_domain is 
  signal r2_thr_hi_net : std_logic_vector( 32-1 downto 0 );
  signal r3_m_net : std_logic_vector( 32-1 downto 0 );
  signal r1_thr_lo_net : std_logic_vector( 32-1 downto 0 );
  signal r4_reset_net : std_logic_vector( 32-1 downto 0 );
begin
  r1_thr_lo_net <= r1_thr_lo;
  r2_thr_hi_net <= r2_thr_hi;
  r3_m_net <= r3_m;
  r4_reset_net <= r4_reset;
end structural;
-- Generated from Simulink block blr/blr_clk_domain/blr/Gate2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_gate2 is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    thr_hi : in std_logic_vector( 32-1 downto 0 );
    thr_lo : in std_logic_vector( 32-1 downto 0 );
    y : out std_logic_vector( 1-1 downto 0 );
    thr_hi_out : out std_logic_vector( 16-1 downto 0 );
    thr_lo_out : out std_logic_vector( 16-1 downto 0 )
  );
end blr_gate2;
architecture structural of blr_gate2 is 
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
  signal register_thr_lo_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 16-1 downto 0 );
  signal slice_y_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal register_thr_hi_q_net : std_logic_vector( 32-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 16-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 16-1 downto 0 );
begin
  y <= logical_y_net;
  thr_hi_out <= reinterpret_output_port_net;
  thr_lo_out <= reinterpret1_output_port_net;
  register_q_net <= x;
  register_thr_hi_q_net <= thr_hi;
  register_thr_lo_q_net <= thr_lo;
  logical : entity xil_defaultlib.sysgen_logical_89f07ef260 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational_op_net,
    d1 => relational1_op_net,
    y => logical_y_net
  );
  relational : entity xil_defaultlib.sysgen_relational_327b214d2f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => reinterpret_output_port_net,
    b => register_q_net,
    op => relational_op_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_327b214d2f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => register_q_net,
    b => reinterpret1_output_port_net,
    op => relational1_op_net
  );
  slice : entity xil_defaultlib.blr_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => register_thr_hi_q_net,
    y => slice_y_net
  );
  slice1 : entity xil_defaultlib.blr_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 15,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => register_thr_lo_q_net,
    y => slice1_y_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_65ee335b25 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice_y_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_65ee335b25 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
end structural;
-- Generated from Simulink block blr/blr_clk_domain/blr
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_blr is
  port (
    x : in std_logic_vector( 16-1 downto 0 );
    thr_hi : in std_logic_vector( 32-1 downto 0 );
    thr_lo : in std_logic_vector( 32-1 downto 0 );
    m : in std_logic_vector( 32-1 downto 0 );
    reset : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    y : out std_logic_vector( 16-1 downto 0 );
    dc_offset : out std_logic_vector( 16-1 downto 0 );
    enable : out std_logic_vector( 1-1 downto 0 );
    thr_lo_out : out std_logic_vector( 16-1 downto 0 );
    thr_hi_out : out std_logic_vector( 16-1 downto 0 )
  );
end blr_blr;
architecture structural of blr_blr is 
  signal slice1_y_net : std_logic_vector( 1-1 downto 0 );
  signal register_q_net : std_logic_vector( 16-1 downto 0 );
  signal register_thr_hi_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_thr_lo_q_net : std_logic_vector( 32-1 downto 0 );
  signal ce_net : std_logic;
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal register_reset_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_m_q_net : std_logic_vector( 32-1 downto 0 );
  signal slice_y_net : std_logic_vector( 2-1 downto 0 );
  signal mcode_y_net : std_logic_vector( 32-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal accumulator1_q_net : std_logic_vector( 32-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal addsub1_s_net : std_logic_vector( 16-1 downto 0 );
begin
  y <= register_q_net;
  dc_offset <= convert1_dout_net;
  enable <= logical_y_net;
  thr_lo_out <= reinterpret1_output_port_net;
  thr_hi_out <= reinterpret_output_port_net;
  x_net <= x;
  register_thr_hi_q_net <= thr_hi;
  register_thr_lo_q_net <= thr_lo;
  register_m_q_net <= m;
  register_reset_q_net <= reset;
  clk_net <= clk_1;
  ce_net <= ce_1;
  gate2 : entity xil_defaultlib.blr_gate2 
  port map (
    x => register_q_net,
    thr_hi => register_thr_hi_q_net,
    thr_lo => register_thr_lo_q_net,
    y => logical_y_net,
    thr_hi_out => reinterpret_output_port_net,
    thr_lo_out => reinterpret1_output_port_net
  );
  slice : entity xil_defaultlib.blr_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 1,
    x_width => 32,
    y_width => 2
  )
  port map (
    x => register_m_q_net,
    y => slice_y_net
  );
  slice1 : entity xil_defaultlib.blr_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 0,
    x_width => 32,
    y_width => 1
  )
  port map (
    x => register_reset_q_net,
    y => slice1_y_net
  );
  accumulator1 : entity xil_defaultlib.sysgen_accum_0d63aba021 
  port map (
    clr => '0',
    b => addsub1_s_net,
    rst => slice1_y_net,
    en => logical_y_net,
    clk => clk_net,
    ce => ce_net,
    q => accumulator1_q_net
  );
  addsub1 : entity xil_defaultlib.blr_xladdsub 
  generic map (
    a_arith => xlSigned,
    a_bin_pt => 14,
    a_width => 16,
    b_arith => xlSigned,
    b_bin_pt => 14,
    b_width => 16,
    c_has_c_out => 0,
    c_latency => 0,
    c_output_width => 17,
    core_name0 => "blr_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 17,
    latency => 0,
    overflow => 2,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 14,
    s_width => 16
  )
  port map (
    clr => '0',
    en => "1",
    a => x_net,
    b => convert1_dout_net,
    clk => clk_net,
    ce => ce_net,
    s => addsub1_s_net
  );
  convert1 : entity xil_defaultlib.blr_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 2,
    din_bin_pt => 14,
    din_width => 32,
    dout_arith => 2,
    dout_bin_pt => 14,
    dout_width => 16,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => mcode_y_net,
    clk => clk_net,
    ce => ce_net,
    dout => convert1_dout_net
  );
  mcode : entity xil_defaultlib.sysgen_mcode_block_7138d4ce61 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    x => accumulator1_q_net,
    m => slice_y_net,
    y => mcode_y_net
  );
  register_x0 : entity xil_defaultlib.blr_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => addsub1_s_net,
    clk => clk_net,
    ce => ce_net,
    q => register_q_net
  );
end structural;
-- Generated from Simulink block blr/blr_clk_domain/format_dac
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_format_dac is
  port (
    y : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out1 : out std_logic_vector( 14-1 downto 0 )
  );
end blr_format_dac;
architecture structural of blr_format_dac is 
  signal ce_net : std_logic;
  signal register_q_net : std_logic_vector( 16-1 downto 0 );
  signal logical_y_net : std_logic_vector( 14-1 downto 0 );
  signal clk_net : std_logic;
  signal negate_op_net : std_logic_vector( 17-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 14-1 downto 0 );
  signal slice_y_net : std_logic_vector( 14-1 downto 0 );
begin
  out1 <= logical_y_net;
  register_q_net <= y;
  clk_net <= clk_1;
  ce_net <= ce_1;
  constant7 : entity xil_defaultlib.sysgen_constant_53d8a08726 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
  logical : entity xil_defaultlib.sysgen_logical_5dabd742fd 
  port map (
    clr => '0',
    d0 => slice_y_net,
    d1 => constant7_op_net,
    clk => clk_net,
    ce => ce_net,
    y => logical_y_net
  );
  negate : entity xil_defaultlib.sysgen_negate_7410b929e5 
  port map (
    clr => '0',
    ip => register_q_net,
    clk => clk_net,
    ce => ce_net,
    op => negate_op_net
  );
  slice : entity xil_defaultlib.blr_xlslice 
  generic map (
    new_lsb => 2,
    new_msb => 15,
    x_width => 17,
    y_width => 14
  )
  port map (
    x => negate_op_net,
    y => slice_y_net
  );
end structural;
-- Generated from Simulink block blr/blr_clk_domain
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_blr_clk_domain is
  port (
    thr_lo : in std_logic_vector( 32-1 downto 0 );
    thr_hi : in std_logic_vector( 32-1 downto 0 );
    m : in std_logic_vector( 32-1 downto 0 );
    reset : in std_logic_vector( 32-1 downto 0 );
    x : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dac_y : out std_logic_vector( 14-1 downto 0 );
    y : out std_logic_vector( 16-1 downto 0 )
  );
end blr_blr_clk_domain;
architecture structural of blr_blr_clk_domain is 
  signal reinterpret1_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 16-1 downto 0 );
  signal logical_y_net_x0 : std_logic_vector( 14-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 16-1 downto 0 );
  signal register_reset_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_thr_hi_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 16-1 downto 0 );
  signal register_m_q_net : std_logic_vector( 32-1 downto 0 );
  signal ce_net : std_logic;
  signal register_thr_lo_q_net : std_logic_vector( 32-1 downto 0 );
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal clk_net : std_logic;
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
begin
  register_thr_lo_q_net <= thr_lo;
  register_thr_hi_q_net <= thr_hi;
  register_m_q_net <= m;
  register_reset_q_net <= reset;
  dac_y <= logical_y_net_x0;
  x_net <= x;
  y <= register_q_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  blr_x0 : entity xil_defaultlib.blr_blr 
  port map (
    x => x_net,
    thr_hi => register_thr_hi_q_net,
    thr_lo => register_thr_lo_q_net,
    m => register_m_q_net,
    reset => register_reset_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    y => register_q_net,
    dc_offset => convert1_dout_net,
    enable => logical_y_net,
    thr_lo_out => reinterpret1_output_port_net,
    thr_hi_out => reinterpret_output_port_net
  );
  format_dac : entity xil_defaultlib.blr_format_dac 
  port map (
    y => register_q_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    out1 => logical_y_net_x0
  );
end structural;
-- Generated from Simulink block blr_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_struct is
  port (
    r1_thr_lo : in std_logic_vector( 32-1 downto 0 );
    r2_thr_hi : in std_logic_vector( 32-1 downto 0 );
    r3_m : in std_logic_vector( 32-1 downto 0 );
    r4_reset : in std_logic_vector( 32-1 downto 0 );
    x : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dac_y : out std_logic_vector( 14-1 downto 0 );
    y : out std_logic_vector( 16-1 downto 0 )
  );
end blr_struct;
architecture structural of blr_struct is 
  signal logical_y_net : std_logic_vector( 14-1 downto 0 );
  signal r1_thr_lo_net : std_logic_vector( 32-1 downto 0 );
  signal register_thr_hi_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_thr_lo_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_m_q_net : std_logic_vector( 32-1 downto 0 );
  signal register_reset_q_net : std_logic_vector( 32-1 downto 0 );
  signal clk_net : std_logic;
  signal x_net : std_logic_vector( 16-1 downto 0 );
  signal r2_thr_hi_net : std_logic_vector( 32-1 downto 0 );
  signal r3_m_net : std_logic_vector( 32-1 downto 0 );
  signal r4_reset_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 16-1 downto 0 );
  signal ce_net : std_logic;
begin
  r1_thr_lo_net <= r1_thr_lo;
  r2_thr_hi_net <= r2_thr_hi;
  r3_m_net <= r3_m;
  r4_reset_net <= r4_reset;
  dac_y <= logical_y_net;
  x_net <= x;
  y <= register_q_net;
  clk_net <= clk_1;
  ce_net <= ce_1;
  axi_clk_domain : entity xil_defaultlib.blr_axi_clk_domain 
  port map (
    r1_thr_lo => r1_thr_lo_net,
    r2_thr_hi => r2_thr_hi_net,
    r3_m => r3_m_net,
    r4_reset => r4_reset_net
  );
  blr_clk_domain : entity xil_defaultlib.blr_blr_clk_domain 
  port map (
    thr_lo => register_thr_lo_q_net,
    thr_hi => register_thr_hi_q_net,
    m => register_m_q_net,
    reset => register_reset_q_net,
    x => x_net,
    clk_1 => clk_net,
    ce_1 => ce_net,
    dac_y => logical_y_net,
    y => register_q_net
  );
  register_thr_hi : entity xil_defaultlib.blr_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000111111010011111011100011100"
  )
  port map (
    en => "1",
    rst => "0",
    d => r2_thr_hi_net,
    clk => clk_net,
    ce => ce_net,
    q => register_thr_hi_q_net
  );
  register_thr_lo : entity xil_defaultlib.blr_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000111110111011101111000010"
  )
  port map (
    en => "1",
    rst => "0",
    d => r1_thr_lo_net,
    clk => clk_net,
    ce => ce_net,
    q => register_thr_lo_q_net
  );
  register_m : entity xil_defaultlib.blr_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000111010011100000011111000"
  )
  port map (
    en => "1",
    rst => "0",
    d => r3_m_net,
    clk => clk_net,
    ce => ce_net,
    q => register_m_q_net
  );
  register_reset : entity xil_defaultlib.blr_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000001"
  )
  port map (
    en => "1",
    rst => "0",
    d => r4_reset_net,
    clk => clk_net,
    ce => ce_net,
    q => register_reset_q_net
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr_default_clock_driver is
  port (
    blr_sysclk : in std_logic;
    blr_sysce : in std_logic;
    blr_sysclr : in std_logic;
    blr_clk1 : out std_logic;
    blr_ce1 : out std_logic
  );
end blr_default_clock_driver;
architecture structural of blr_default_clock_driver is 
begin
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => blr_sysclk,
    sysce => blr_sysce,
    sysclr => blr_sysclr,
    clk => blr_clk1,
    ce => blr_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity blr is
  port (
    r1_thr_lo : in std_logic_vector( 32-1 downto 0 );
    r2_thr_hi : in std_logic_vector( 32-1 downto 0 );
    r3_m : in std_logic_vector( 32-1 downto 0 );
    r4_reset : in std_logic_vector( 32-1 downto 0 );
    x : in std_logic_vector( 16-1 downto 0 );
    clk : in std_logic;
    dac_y : out std_logic_vector( 14-1 downto 0 );
    y : out std_logic_vector( 16-1 downto 0 )
  );
end blr;
architecture structural of blr is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "blr,sysgen_core_2022_2,{,compilation=IP Catalog,block_icon_display=Default,family=zynq,part=xc7z020,speed=-1,package=clg484,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=0,interface_doc=1,ce_clr=0,clock_period=10,system_simulink_period=2e-08,waveform_viewer=0,axilite_interface=0,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=0.01,accum=1,addsub=1,constant=1,convert=1,logical=2,mcode=1,negate=1,register=5,reinterpret=2,relational=2,slice=5,}";
  signal ce_1_net : std_logic;
  signal clk_1_net : std_logic;
begin
  blr_default_clock_driver : entity xil_defaultlib.blr_default_clock_driver 
  port map (
    blr_sysclk => clk,
    blr_sysce => '1',
    blr_sysclr => '0',
    blr_clk1 => clk_1_net,
    blr_ce1 => ce_1_net
  );
  blr_struct : entity xil_defaultlib.blr_struct 
  port map (
    r1_thr_lo => r1_thr_lo,
    r2_thr_hi => r2_thr_hi,
    r3_m => r3_m,
    r4_reset => r4_reset,
    x => x,
    clk_1 => clk_1_net,
    ce_1 => ce_1_net,
    dac_y => dac_y,
    y => y
  );
end structural;
