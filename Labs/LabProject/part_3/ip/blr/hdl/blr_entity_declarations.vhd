-------------------------------------------------------------------
-- System Generator version 2022.2 VHDL source file.
--
-- Copyright(C) 2022 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2022 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlregister.vhd
--
--  Description   : VHDL description of an arbitrary wide register.
--                  Unlike the delay block, an initial value is
--                  specified and is considered valid at the start
--                  of simulation.  The register is only one word
--                  deep.
--
--  Mod. History  : Removed valid bit logic from wrapper.
--                : Changed VHDL to use a bit_vector generic for its
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity blr_xlregister is

   generic (d_width          : integer := 5;          -- Width of d input
            init_value       : bit_vector := b"00");  -- Binary init value string

   port (d   : in std_logic_vector (d_width-1 downto 0);
         rst : in std_logic_vector(0 downto 0) := "0";
         en  : in std_logic_vector(0 downto 0) := "1";
         ce  : in std_logic;
         clk : in std_logic;
         q   : out std_logic_vector (d_width-1 downto 0));

end blr_xlregister;

architecture behavior of blr_xlregister is

   component synth_reg_w_init
      generic (width      : integer;
               init_index : integer;
               init_value : bit_vector;
               latency    : integer);
      port (i   : in std_logic_vector(width-1 downto 0);
            ce  : in std_logic;
            clr : in std_logic;
            clk : in std_logic;
            o   : out std_logic_vector(width-1 downto 0));
   end component; -- end synth_reg_w_init

   -- synthesis translate_off
   signal real_d, real_q           : real;    -- For debugging info ports
   -- synthesis translate_on
   signal internal_clr             : std_logic;
   signal internal_ce              : std_logic;

begin

   internal_clr <= rst(0) and ce;
   internal_ce  <= en(0) and ce;

   -- Synthesizable behavioral model
   synth_reg_inst : synth_reg_w_init
      generic map (width      => d_width,
                   init_index => 2,
                   init_value => init_value,
                   latency    => 1)
      port map (i   => d,
                ce  => internal_ce,
                clr => internal_clr,
                clk => clk,
                o   => q);

end architecture behavior;


library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

---------------------------------------------------------------------
--
--  Filename      : xlslice.vhd
--
--  Description   : VHDL description of a block that sets the output to a
--                  specified range of the input bits. The output is always
--                  set to an unsigned type with it's binary point at zero.
--
---------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity blr_xlslice is
    generic (
        new_msb      : integer := 9;           -- position of new msb
        new_lsb      : integer := 1;           -- position of new lsb
        x_width      : integer := 16;          -- Width of x input
        y_width      : integer := 8);          -- Width of y output
    port (
        x : in std_logic_vector (x_width-1 downto 0);
        y : out std_logic_vector (y_width-1 downto 0));
end blr_xlslice;

architecture behavior of blr_xlslice is
begin
    y <= x(new_msb downto new_lsb);
end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_accum_0d63aba021 is
  port (
    b : in std_logic_vector((16 - 1) downto 0);
    rst : in std_logic_vector((1 - 1) downto 0);
    en : in std_logic_vector((1 - 1) downto 0);
    q : out std_logic_vector((32 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_accum_0d63aba021;
architecture behavior of sysgen_accum_0d63aba021
is
  signal b_17_24: signed((16 - 1) downto 0);
  signal rst_17_27: boolean;
  signal en_17_32: boolean;
  signal accum_reg_39_23: signed((32 - 1) downto 0) := "00000000000000000000000000000000";
  signal accum_reg_39_23_rst: std_logic;
  signal accum_reg_39_23_en: std_logic;
  signal cast_49_42: signed((32 - 1) downto 0);
  signal accum_reg_join_45_1: signed((33 - 1) downto 0);
  signal accum_reg_join_45_1_en: std_logic;
  signal accum_reg_join_45_1_rst: std_logic;
begin
  b_17_24 <= std_logic_vector_to_signed(b);
  rst_17_27 <= ((rst) = "1");
  en_17_32 <= ((en) = "1");
  proc_accum_reg_39_23: process (clk)
  is
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (accum_reg_39_23_rst = '1')) then
        accum_reg_39_23 <= "00000000000000000000000000000000";
      elsif ((ce = '1') and (accum_reg_39_23_en = '1')) then 
        accum_reg_39_23 <= accum_reg_39_23 + cast_49_42;
      end if;
    end if;
  end process proc_accum_reg_39_23;
  cast_49_42 <= s2s_cast(b_17_24, 14, 32, 14);
  proc_if_45_1: process (accum_reg_39_23, cast_49_42, en_17_32, rst_17_27)
  is
  begin
    if rst_17_27 then
      accum_reg_join_45_1_rst <= '1';
    elsif en_17_32 then
      accum_reg_join_45_1_rst <= '0';
    else 
      accum_reg_join_45_1_rst <= '0';
    end if;
    if en_17_32 then
      accum_reg_join_45_1_en <= '1';
    else 
      accum_reg_join_45_1_en <= '0';
    end if;
  end process proc_if_45_1;
  accum_reg_39_23_rst <= accum_reg_join_45_1_rst;
  accum_reg_39_23_en <= accum_reg_join_45_1_en;
  q <= signed_to_std_logic_vector(accum_reg_39_23);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

--$Header: /devl/xcs/repo/env/Jobs/sysgen/src/xbs/blocks/xlconvert/hdl/xlconvert.vhd,v 1.1 2004/11/22 00:17:30 rosty Exp $
---------------------------------------------------------------------
--
--  Filename      : xlconvert.vhd
--
--  Description   : VHDL description of a fixed point converter block that
--                  converts the input to a new output type.

--
---------------------------------------------------------------------


---------------------------------------------------------------------
--
--  Entity        : xlconvert
--
--  Architecture  : behavior
--
--  Description   : Top level VHDL description of fixed point conver block.
--
---------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity convert_func_call_blr_xlconvert is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        result : out std_logic_vector (dout_width-1 downto 0));
end convert_func_call_blr_xlconvert ;

architecture behavior of convert_func_call_blr_xlconvert is
begin
    -- Convert to output type and do saturation arith.
    result <= convert_type(din, din_width, din_bin_pt, din_arith,
                           dout_width, dout_bin_pt, dout_arith,
                           quantization, overflow);
end behavior;


library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;


entity blr_xlconvert  is
    generic (
        din_width    : integer := 16;            -- Width of input
        din_bin_pt   : integer := 4;             -- Binary point of input
        din_arith    : integer := xlUnsigned;    -- Type of arith of input
        dout_width   : integer := 8;             -- Width of output
        dout_bin_pt  : integer := 2;             -- Binary point of output
        dout_arith   : integer := xlUnsigned;    -- Type of arith of output
        en_width     : integer := 1;
        en_bin_pt    : integer := 0;
        en_arith     : integer := xlUnsigned;
        bool_conversion : integer :=0;           -- if one, convert ufix_1_0 to
                                                 -- bool
        latency      : integer := 0;             -- Ouput delay clk cycles
        quantization : integer := xlTruncate;    -- xlRound or xlTruncate
        overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
    port (
        din : in std_logic_vector (din_width-1 downto 0);
        en  : in std_logic_vector (en_width-1 downto 0);
        ce  : in std_logic;
        clr : in std_logic;
        clk : in std_logic;
        dout : out std_logic_vector (dout_width-1 downto 0));

end blr_xlconvert ;

architecture behavior of blr_xlconvert  is

    component synth_reg
        generic (width       : integer;
                 latency     : integer);
        port (i       : in std_logic_vector(width-1 downto 0);
              ce      : in std_logic;
              clr     : in std_logic;
              clk     : in std_logic;
              o       : out std_logic_vector(width-1 downto 0));
    end component;

    component convert_func_call_blr_xlconvert 
        generic (
            din_width    : integer := 16;            -- Width of input
            din_bin_pt   : integer := 4;             -- Binary point of input
            din_arith    : integer := xlUnsigned;    -- Type of arith of input
            dout_width   : integer := 8;             -- Width of output
            dout_bin_pt  : integer := 2;             -- Binary point of output
            dout_arith   : integer := xlUnsigned;    -- Type of arith of output
            quantization : integer := xlTruncate;    -- xlRound or xlTruncate
            overflow     : integer := xlWrap);       -- xlSaturate or xlWrap
        port (
            din : in std_logic_vector (din_width-1 downto 0);
            result : out std_logic_vector (dout_width-1 downto 0));
    end component;


    -- synthesis translate_off
--    signal real_din, real_dout : real;    -- For debugging info ports
    -- synthesis translate_on
    signal result : std_logic_vector(dout_width-1 downto 0);
    signal internal_ce : std_logic;

begin

    -- Debugging info for internal full precision variables
    -- synthesis translate_off
--     real_din <= to_real(din, din_bin_pt, din_arith);
--     real_dout <= to_real(dout, dout_bin_pt, dout_arith);
    -- synthesis translate_on

    internal_ce <= ce and en(0);

    bool_conversion_generate : if (bool_conversion = 1)
    generate
      result <= din;
    end generate; --bool_conversion_generate

    std_conversion_generate : if (bool_conversion = 0)
    generate
      -- Workaround for XST bug
      convert : convert_func_call_blr_xlconvert 
        generic map (
          din_width   => din_width,
          din_bin_pt  => din_bin_pt,
          din_arith   => din_arith,
          dout_width  => dout_width,
          dout_bin_pt => dout_bin_pt,
          dout_arith  => dout_arith,
          quantization => quantization,
          overflow     => overflow)
        port map (
          din => din,
          result => result);
    end generate; --std_conversion_generate

    latency_test : if (latency > 0) generate
        reg : synth_reg
            generic map (
              width => dout_width,
              latency => latency
            )
            port map (
              i => result,
              ce => internal_ce,
              clr => clr,
              clk => clk,
              o => dout
            );
    end generate;

    latency0 : if (latency = 0)
    generate
        dout <= result;
    end generate latency0;

end  behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_mcode_block_7138d4ce61 is
  port (
    x : in std_logic_vector((32 - 1) downto 0);
    m : in std_logic_vector((2 - 1) downto 0);
    y : out std_logic_vector((32 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_mcode_block_7138d4ce61;
architecture behavior of sysgen_mcode_block_7138d4ce61
is
  signal x_1_30: signed((32 - 1) downto 0);
  signal m_1_33: unsigned((2 - 1) downto 0);
  signal sign_20_5_slice: unsigned((1 - 1) downto 0);
  signal slice_36_42: unsigned((15 - 1) downto 0);
  signal y16n_36_5_concat: unsigned((32 - 1) downto 0);
  signal slice_37_42: unsigned((19 - 1) downto 0);
  signal y12n_37_5_concat: unsigned((32 - 1) downto 0);
  signal slice_38_42: unsigned((22 - 1) downto 0);
  signal y9n_38_5_concat: unsigned((32 - 1) downto 0);
  signal slice_39_42: unsigned((25 - 1) downto 0);
  signal y6n_39_5_concat: unsigned((32 - 1) downto 0);
  signal slice_41_42: unsigned((15 - 1) downto 0);
  signal y16p_41_5_concat: unsigned((32 - 1) downto 0);
  signal slice_42_42: unsigned((19 - 1) downto 0);
  signal y12p_42_5_concat: unsigned((32 - 1) downto 0);
  signal slice_43_42: unsigned((22 - 1) downto 0);
  signal y9p_43_5_concat: unsigned((32 - 1) downto 0);
  signal slice_44_42: unsigned((25 - 1) downto 0);
  signal y6p_44_5_concat: unsigned((32 - 1) downto 0);
  signal y_49_13_force: signed((32 - 1) downto 0);
  signal y_51_13_force: signed((32 - 1) downto 0);
  signal y_53_13_force: signed((32 - 1) downto 0);
  signal y_55_13_force: signed((32 - 1) downto 0);
  signal y_57_13_force: signed((32 - 1) downto 0);
  signal y_59_13_force: signed((32 - 1) downto 0);
  signal y_62_13_force: signed((32 - 1) downto 0);
  signal y_64_13_force: signed((32 - 1) downto 0);
  signal rel_61_12: boolean;
  signal rel_61_22: boolean;
  signal bool_61_12: boolean;
  signal y_join_61_9: signed((32 - 1) downto 0);
  signal rel_48_8: boolean;
  signal rel_48_18: boolean;
  signal bool_48_8: boolean;
  signal rel_50_12: boolean;
  signal rel_50_22: boolean;
  signal bool_50_12: boolean;
  signal rel_52_12: boolean;
  signal rel_52_22: boolean;
  signal bool_52_12: boolean;
  signal rel_54_12: boolean;
  signal rel_54_22: boolean;
  signal bool_54_12: boolean;
  signal rel_56_12: boolean;
  signal rel_56_22: boolean;
  signal bool_56_12: boolean;
  signal rel_58_12: boolean;
  signal rel_58_22: boolean;
  signal bool_58_12: boolean;
  signal y_join_48_5: signed((32 - 1) downto 0);
begin
  x_1_30 <= std_logic_vector_to_signed(x);
  m_1_33 <= std_logic_vector_to_unsigned(m);
  sign_20_5_slice <= s2u_slice(x_1_30, 31, 31);
  slice_36_42 <= s2u_slice(x_1_30, 30, 16);
  y16n_36_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("1111111111111111")) & unsigned_to_std_logic_vector(slice_36_42));
  slice_37_42 <= s2u_slice(x_1_30, 30, 12);
  y12n_37_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("111111111111")) & unsigned_to_std_logic_vector(slice_37_42));
  slice_38_42 <= s2u_slice(x_1_30, 30, 9);
  y9n_38_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("111111111")) & unsigned_to_std_logic_vector(slice_38_42));
  slice_39_42 <= s2u_slice(x_1_30, 30, 6);
  y6n_39_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("111111")) & unsigned_to_std_logic_vector(slice_39_42));
  slice_41_42 <= s2u_slice(x_1_30, 30, 16);
  y16p_41_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("0000000000000000")) & unsigned_to_std_logic_vector(slice_41_42));
  slice_42_42 <= s2u_slice(x_1_30, 30, 12);
  y12p_42_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("000000000000")) & unsigned_to_std_logic_vector(slice_42_42));
  slice_43_42 <= s2u_slice(x_1_30, 30, 9);
  y9p_43_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("000000000")) & unsigned_to_std_logic_vector(slice_43_42));
  slice_44_42 <= s2u_slice(x_1_30, 30, 6);
  y6p_44_5_concat <= std_logic_vector_to_unsigned(unsigned_to_std_logic_vector(sign_20_5_slice) & unsigned_to_std_logic_vector(std_logic_vector_to_unsigned("000000")) & unsigned_to_std_logic_vector(slice_44_42));
  y_49_13_force <= unsigned_to_signed(y6p_44_5_concat);
  y_51_13_force <= unsigned_to_signed(y6n_39_5_concat);
  y_53_13_force <= unsigned_to_signed(y9p_43_5_concat);
  y_55_13_force <= unsigned_to_signed(y9n_38_5_concat);
  y_57_13_force <= unsigned_to_signed(y12p_42_5_concat);
  y_59_13_force <= unsigned_to_signed(y12n_37_5_concat);
  y_62_13_force <= unsigned_to_signed(y16p_41_5_concat);
  y_64_13_force <= unsigned_to_signed(y16n_36_5_concat);
  rel_61_12 <= m_1_33 = std_logic_vector_to_unsigned("11");
  rel_61_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("0");
  bool_61_12 <= rel_61_12 and rel_61_22;
  proc_if_61_9: process (bool_61_12, y_62_13_force, y_64_13_force)
  is
  begin
    if bool_61_12 then
      y_join_61_9 <= y_62_13_force;
    else 
      y_join_61_9 <= y_64_13_force;
    end if;
  end process proc_if_61_9;
  rel_48_8 <= m_1_33 = std_logic_vector_to_unsigned("00");
  rel_48_18 <= sign_20_5_slice = std_logic_vector_to_unsigned("0");
  bool_48_8 <= rel_48_8 and rel_48_18;
  rel_50_12 <= m_1_33 = std_logic_vector_to_unsigned("00");
  rel_50_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("1");
  bool_50_12 <= rel_50_12 and rel_50_22;
  rel_52_12 <= m_1_33 = std_logic_vector_to_unsigned("01");
  rel_52_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("0");
  bool_52_12 <= rel_52_12 and rel_52_22;
  rel_54_12 <= m_1_33 = std_logic_vector_to_unsigned("01");
  rel_54_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("1");
  bool_54_12 <= rel_54_12 and rel_54_22;
  rel_56_12 <= m_1_33 = std_logic_vector_to_unsigned("10");
  rel_56_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("0");
  bool_56_12 <= rel_56_12 and rel_56_22;
  rel_58_12 <= m_1_33 = std_logic_vector_to_unsigned("10");
  rel_58_22 <= sign_20_5_slice = std_logic_vector_to_unsigned("1");
  bool_58_12 <= rel_58_12 and rel_58_22;
  proc_if_48_5: process (bool_48_8, bool_50_12, bool_52_12, bool_54_12, bool_56_12, bool_58_12, y_49_13_force, y_51_13_force, y_53_13_force, y_55_13_force, y_57_13_force, y_59_13_force, y_join_61_9)
  is
  begin
    if bool_48_8 then
      y_join_48_5 <= y_49_13_force;
    elsif bool_50_12 then
      y_join_48_5 <= y_51_13_force;
    elsif bool_52_12 then
      y_join_48_5 <= y_53_13_force;
    elsif bool_54_12 then
      y_join_48_5 <= y_55_13_force;
    elsif bool_56_12 then
      y_join_48_5 <= y_57_13_force;
    elsif bool_58_12 then
      y_join_48_5 <= y_59_13_force;
    else 
      y_join_48_5 <= y_join_61_9;
    end if;
  end process proc_if_48_5;
  y <= signed_to_std_logic_vector(y_join_48_5);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_logical_89f07ef260 is
  port (
    d0 : in std_logic_vector((1 - 1) downto 0);
    d1 : in std_logic_vector((1 - 1) downto 0);
    y : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_logical_89f07ef260;
architecture behavior of sysgen_logical_89f07ef260
is
  signal d0_1_24: std_logic;
  signal d1_1_27: std_logic;
  signal fully_2_1_bit: std_logic;
begin
  d0_1_24 <= d0(0);
  d1_1_27 <= d1(0);
  fully_2_1_bit <= d0_1_24 and d1_1_27;
  y <= std_logic_to_vector(fully_2_1_bit);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_relational_327b214d2f is
  port (
    a : in std_logic_vector((16 - 1) downto 0);
    b : in std_logic_vector((16 - 1) downto 0);
    op : out std_logic_vector((1 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_relational_327b214d2f;
architecture behavior of sysgen_relational_327b214d2f
is
  signal a_1_31: signed((16 - 1) downto 0);
  signal b_1_34: signed((16 - 1) downto 0);
  signal result_18_3_rel: boolean;
begin
  a_1_31 <= std_logic_vector_to_signed(a);
  b_1_34 <= std_logic_vector_to_signed(b);
  result_18_3_rel <= a_1_31 > b_1_34;
  op <= boolean_to_vector(result_18_3_rel);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_reinterpret_65ee335b25 is
  port (
    input_port : in std_logic_vector((16 - 1) downto 0);
    output_port : out std_logic_vector((16 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_reinterpret_65ee335b25;
architecture behavior of sysgen_reinterpret_65ee335b25
is
  signal input_port_1_40: unsigned((16 - 1) downto 0);
  signal output_port_5_5_force: signed((16 - 1) downto 0);
begin
  input_port_1_40 <= std_logic_vector_to_unsigned(input_port);
  output_port_5_5_force <= unsigned_to_signed(input_port_1_40);
  output_port <= signed_to_std_logic_vector(output_port_5_5_force);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_constant_53d8a08726 is
  port (
    op : out std_logic_vector((14 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_constant_53d8a08726;
architecture behavior of sysgen_constant_53d8a08726
is
begin
  op <= "10000000000000";
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_logical_5dabd742fd is
  port (
    d0 : in std_logic_vector((14 - 1) downto 0);
    d1 : in std_logic_vector((14 - 1) downto 0);
    y : out std_logic_vector((14 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_logical_5dabd742fd;
architecture behavior of sysgen_logical_5dabd742fd
is
  signal d0_1_24: std_logic_vector((14 - 1) downto 0);
  signal d1_1_27: std_logic_vector((14 - 1) downto 0);
  type array_type_latency_pipe_5_26 is array (0 to (1 - 1)) of std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26: array_type_latency_pipe_5_26 := (
    0 => "00000000000000");
  signal latency_pipe_5_26_front_din: std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26_back: std_logic_vector((14 - 1) downto 0);
  signal latency_pipe_5_26_push_front_pop_back_en: std_logic;
  signal fully_2_1_bit: std_logic_vector((14 - 1) downto 0);
begin
  d0_1_24 <= d0;
  d1_1_27 <= d1;
  latency_pipe_5_26_back <= latency_pipe_5_26(0);
  proc_latency_pipe_5_26: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (latency_pipe_5_26_push_front_pop_back_en = '1')) then
        latency_pipe_5_26(0) <= latency_pipe_5_26_front_din;
      end if;
    end if;
  end process proc_latency_pipe_5_26;
  fully_2_1_bit <= d0_1_24 xor d1_1_27;
  latency_pipe_5_26_front_din <= fully_2_1_bit;
  latency_pipe_5_26_push_front_pop_back_en <= '1';
  y <= latency_pipe_5_26_back;
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity sysgen_negate_7410b929e5 is
  port (
    ip : in std_logic_vector((16 - 1) downto 0);
    op : out std_logic_vector((17 - 1) downto 0);
    clk : in std_logic;
    ce : in std_logic;
    clr : in std_logic);
end sysgen_negate_7410b929e5;
architecture behavior of sysgen_negate_7410b929e5
is
  signal ip_18_25: signed((16 - 1) downto 0);
  type array_type_op_mem_48_20 is array (0 to (1 - 1)) of signed((17 - 1) downto 0);
  signal op_mem_48_20: array_type_op_mem_48_20 := (
    0 => "00000000000000000");
  signal op_mem_48_20_front_din: signed((17 - 1) downto 0);
  signal op_mem_48_20_back: signed((17 - 1) downto 0);
  signal op_mem_48_20_push_front_pop_back_en: std_logic;
  signal cast_35_24: signed((17 - 1) downto 0);
  signal internal_ip_35_9_neg: signed((17 - 1) downto 0);
  signal internal_ip_join_30_1: signed((17 - 1) downto 0);
begin
  ip_18_25 <= std_logic_vector_to_signed(ip);
  op_mem_48_20_back <= op_mem_48_20(0);
  proc_op_mem_48_20: process (clk)
  is
    variable i: integer;
  begin
    if (clk'event and (clk = '1')) then
      if ((ce = '1') and (op_mem_48_20_push_front_pop_back_en = '1')) then
        op_mem_48_20(0) <= op_mem_48_20_front_din;
      end if;
    end if;
  end process proc_op_mem_48_20;
  cast_35_24 <= s2s_cast(ip_18_25, 14, 17, 14);
  internal_ip_35_9_neg <=  -cast_35_24;
  proc_if_30_1: process (internal_ip_35_9_neg)
  is
  begin
    if false then
      internal_ip_join_30_1 <= std_logic_vector_to_signed("00000000000000000");
    else 
      internal_ip_join_30_1 <= internal_ip_35_9_neg;
    end if;
  end process proc_if_30_1;
  op_mem_48_20_front_din <= internal_ip_join_30_1;
  op_mem_48_20_push_front_pop_back_en <= '1';
  op <= signed_to_std_logic_vector(op_mem_48_20_back);
end behavior;

library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;

-------------------------------------------------------------------
 -- System Generator VHDL source file.
 --
 -- Copyright(C) 2018 by Xilinx, Inc.  All rights reserved.  This
 -- text/file contains proprietary, confidential information of Xilinx,
 -- Inc., is distributed under license from Xilinx, Inc., and may be used,
 -- copied and/or disclosed only pursuant to the terms of a valid license
 -- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
 -- this text/file solely for design, simulation, implementation and
 -- creation of design files limited to Xilinx devices or technologies.
 -- Use with non-Xilinx devices or technologies is expressly prohibited
 -- and immediately terminates your license unless covered by a separate
 -- agreement.
 --
 -- Xilinx is providing this design, code, or information "as is" solely
 -- for use in developing programs and solutions for Xilinx devices.  By
 -- providing this design, code, or information as one possible
 -- implementation of this feature, application or standard, Xilinx is
 -- making no representation that this implementation is free from any
 -- claims of infringement.  You are responsible for obtaining any rights
 -- you may require for your implementation.  Xilinx expressly disclaims
 -- any warranty whatsoever with respect to the adequacy of the
 -- implementation, including but not limited to warranties of
 -- merchantability or fitness for a particular purpose.
 --
 -- Xilinx products are not intended for use in life support appliances,
 -- devices, or systems.  Use in such applications is expressly prohibited.
 --
 -- Any modifications that are made to the source code are done at the user's
 -- sole risk and will be unsupported.
 --
 -- This copyright and support notice must be retained as part of this
 -- text at all times.  (c) Copyright 1995-2018 Xilinx, Inc.  All rights
 -- reserved.
 -------------------------------------------------------------------
 library IEEE;
 use IEEE.std_logic_1164.all;
 use IEEE.std_logic_arith.all;

entity blr_xladdsub is 
   generic (
     core_name0: string := "";
     a_width: integer := 16;
     a_bin_pt: integer := 4;
     a_arith: integer := xlUnsigned;
     c_in_width: integer := 16;
     c_in_bin_pt: integer := 4;
     c_in_arith: integer := xlUnsigned;
     c_out_width: integer := 16;
     c_out_bin_pt: integer := 4;
     c_out_arith: integer := xlUnsigned;
     b_width: integer := 8;
     b_bin_pt: integer := 2;
     b_arith: integer := xlUnsigned;
     s_width: integer := 17;
     s_bin_pt: integer := 4;
     s_arith: integer := xlUnsigned;
     rst_width: integer := 1;
     rst_bin_pt: integer := 0;
     rst_arith: integer := xlUnsigned;
     en_width: integer := 1;
     en_bin_pt: integer := 0;
     en_arith: integer := xlUnsigned;
     full_s_width: integer := 17;
     full_s_arith: integer := xlUnsigned;
     mode: integer := xlAddMode;
     extra_registers: integer := 0;
     latency: integer := 0;
     quantization: integer := xlTruncate;
     overflow: integer := xlWrap;
     c_latency: integer := 0;
     c_output_width: integer := 17;
     c_has_c_in : integer := 0;
     c_has_c_out : integer := 0
   );
   port (
     a: in std_logic_vector(a_width - 1 downto 0);
     b: in std_logic_vector(b_width - 1 downto 0);
     c_in : in std_logic_vector (0 downto 0) := "0";
     ce: in std_logic;
     clr: in std_logic := '0';
     clk: in std_logic;
     rst: in std_logic_vector(rst_width - 1 downto 0) := "0";
     en: in std_logic_vector(en_width - 1 downto 0) := "1";
     c_out : out std_logic_vector (0 downto 0);
     s: out std_logic_vector(s_width - 1 downto 0)
   );
 end blr_xladdsub;
 
 architecture behavior of blr_xladdsub is 
 component synth_reg
 generic (
 width: integer := 16;
 latency: integer := 5
 );
 port (
 i: in std_logic_vector(width - 1 downto 0);
 ce: in std_logic;
 clr: in std_logic;
 clk: in std_logic;
 o: out std_logic_vector(width - 1 downto 0)
 );
 end component;
 
 function format_input(inp: std_logic_vector; old_width, delta, new_arith,
 new_width: integer)
 return std_logic_vector
 is
 variable vec: std_logic_vector(old_width-1 downto 0);
 variable padded_inp: std_logic_vector((old_width + delta)-1 downto 0);
 variable result: std_logic_vector(new_width-1 downto 0);
 begin
 vec := inp;
 if (delta > 0) then
 padded_inp := pad_LSB(vec, old_width+delta);
 result := extend_MSB(padded_inp, new_width, new_arith);
 else
 result := extend_MSB(vec, new_width, new_arith);
 end if;
 return result;
 end;
 
 constant full_s_bin_pt: integer := fractional_bits(a_bin_pt, b_bin_pt);
 constant full_a_width: integer := full_s_width;
 constant full_b_width: integer := full_s_width;
 
 signal full_a: std_logic_vector(full_a_width - 1 downto 0);
 signal full_b: std_logic_vector(full_b_width - 1 downto 0);
 signal core_s: std_logic_vector(full_s_width - 1 downto 0);
 signal conv_s: std_logic_vector(s_width - 1 downto 0);
 signal temp_cout : std_logic;
 signal internal_clr: std_logic;
 signal internal_ce: std_logic;
 signal extra_reg_ce: std_logic;
 signal override: std_logic;
 signal logic1: std_logic_vector(0 downto 0);


 component blr_c_addsub_v12_0_i0
    port ( 
    a: in std_logic_vector(17 - 1 downto 0);
    s: out std_logic_vector(c_output_width - 1 downto 0);
    b: in std_logic_vector(17 - 1 downto 0) 
 		  ); 
 end component;

begin
 internal_clr <= (clr or (rst(0))) and ce;
 internal_ce <= ce and en(0);
 logic1(0) <= '1';
 addsub_process: process (a, b, core_s)
 begin
 full_a <= format_input (a, a_width, b_bin_pt - a_bin_pt, a_arith,
 full_a_width);
 full_b <= format_input (b, b_width, a_bin_pt - b_bin_pt, b_arith,
 full_b_width);
 conv_s <= convert_type (core_s, full_s_width, full_s_bin_pt, full_s_arith,
 s_width, s_bin_pt, s_arith, quantization, overflow);
 end process addsub_process;


 comp0: if ((core_name0 = "blr_c_addsub_v12_0_i0")) generate 
  core_instance0:blr_c_addsub_v12_0_i0
   port map ( 
         a => full_a,
         s => core_s,
         b => full_b
  ); 
   end generate;

latency_test: if (extra_registers > 0) generate
 override_test: if (c_latency > 1) generate
 override_pipe: synth_reg
 generic map (
 width => 1,
 latency => c_latency
 )
 port map (
 i => logic1,
 ce => internal_ce,
 clr => internal_clr,
 clk => clk,
 o(0) => override);
 extra_reg_ce <= ce and en(0) and override;
 end generate override_test;
 no_override: if ((c_latency = 0) or (c_latency = 1)) generate
 extra_reg_ce <= ce and en(0);
 end generate no_override;
 extra_reg: synth_reg
 generic map (
 width => s_width,
 latency => extra_registers
 )
 port map (
 i => conv_s,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => s
 );
 cout_test: if (c_has_c_out = 1) generate
 c_out_extra_reg: synth_reg
 generic map (
 width => 1,
 latency => extra_registers
 )
 port map (
 i(0) => temp_cout,
 ce => extra_reg_ce,
 clr => internal_clr,
 clk => clk,
 o => c_out
 );
 end generate cout_test;
 end generate;
 
 latency_s: if ((latency = 0) or (extra_registers = 0)) generate
 s <= conv_s;
 end generate latency_s;
 latency0: if (((latency = 0) or (extra_registers = 0)) and
 (c_has_c_out = 1)) generate
 c_out(0) <= temp_cout;
 end generate latency0;
 tie_dangling_cout: if (c_has_c_out = 0) generate
 c_out <= "0";
 end generate tie_dangling_cout;
 end architecture behavior;

