{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab Project 1\n",
    "# Part 1: Pulse Acquisition and Detector Characterization\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "This Jupyter Notebook will provide you a starting point to interface with the data acquisition system (DAQ) you have already deployed from Vivado/Vitis into the ZedBoard. The DAQ analog frontend (AFE) is provided with a 50 Msps A/D converter (ADC) and a 50 Msps D/A converter (DAC).\n",
    "\n",
    "In this stage of the project, the AFE will work as follows:\n",
    "- The ADC digitizes the raw signal from the detector\n",
    "- The DAC provides a signal loopback to assess the correctness of the digitalization and data representation within the FPGA fabric.\n",
    "\n",
    "Before starting, you should have previously finished the following steps:\n",
    "\n",
    "    [x] Deploying a trigger-based digitalization design (oscilloscope) using the ComBlock in the SoC PL (Vivado)\n",
    "    [x] Executing the UDMA server firmware in the SoC PS (Vitis) \n",
    "\n",
    "## Goals\n",
    "The main goal of this exercise (Lab Project Part 1) is:\n",
    "\n",
    "    - Computing the parameters that better fit the behavior of the detector, based on the pulse model\n",
    "\n",
    "To do so, the following secondary goals shall be achieved:  \n",
    "\n",
    "    - Acquire and record a dataset of multiple raw pulses from the detector, using the SoC/FPGA DAQ via UDMA\n",
    "    - Clean up the dataset to remove unwanted pulses and restore their baseline\n",
    "    - Fit the recorded pulses to get the average parameters that better represent the ideal pulse model of the detector\n",
    "\n",
    "## Procedure\n",
    "You will have the opportunity to demonstrate the gained knowledge throughout the previous laboratory sessions. We will provide you some hints to guide you through the steps to achieve the goals; however, the instructions are limited, leaving you the chance to write the code from scratch with the help of a couple of high-abstraction level libraries.\n",
    "\n",
    "You may use the guides from previous exercises as auxiliary material, especially the one from Lab 4. The recommended steps to follow are divided in two sections as follows:\n",
    "\n",
    "### A) Data acquisition\n",
    "- Connect to the ZedBoard via UDMA\n",
    "- Disable the UDMA logging to speed up the data acquisition\n",
    "- Setup initial acquisition parameters, using the ComBlock output registers\n",
    "  - Threshold level to trigger the capture of a pulse\n",
    "  - Number of samples to capture before the trigger\n",
    "  - Length of the trace to record\n",
    "- Record $N$ pulses via UDMA using the _pulsefit_ library\n",
    "  - Verify that the recorded dataset matches your expectations\n",
    "  - Change the acquisition parameters and repeat the recording, if necessary\n",
    "\n",
    "### B) Data analysis\n",
    "#### B.1) Data wrangling\n",
    "- Clean up (remove) the saturated pulses\n",
    "- Restore the baseline of the remaining pulses\n",
    "- Leave only high-energy pulses, to reduce the fitting uncertainty\n",
    "\n",
    "#### B.2) Pulse fitting - detector characterization\n",
    "- Fit each individual pulse in the dataset using the _pulsefit_ library\n",
    "- Obtain the average parameter values that better represent the detector behavior:\n",
    "  - `tauDecay` ($\\tau_D$)\n",
    "  - `tauRise` ($\\tau_R$)\n",
    "  - `timeArrival` ($t_0$)\n",
    "\n",
    "\n",
    "---\n",
    "\n",
    "- Author(s): Ivan Morales (MLab/ICTP) - 2023/11/20 (version 1.0)\n",
    "- Update(s):\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Before starting\n",
    "\n",
    "🔴 All the instructions that are intended for you to code from scratch will be shown as follows: 🔴\n",
    "> Instruction to follow\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Importing libraries and initializing the detector parameters. Do not change any value if you are not sure of what they mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from pulsefit import PulseFit\n",
    "import udma\n",
    "import time\n",
    "\n",
    "# Total trace length (samples). This value must match the Vivado Block Design settings.\n",
    "TRACE_LEN = 2048     # Number of samples to be recorded\n",
    "SAMPLING_RATE = 50e6 # Sampling rate (fs) of the ADC (50 Msps)\n",
    "ADC_RESOLUTION = 14  # Resolution of the ADC\n",
    "DC_OFFSET = (2**ADC_RESOLUTION // 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A) Data acquisition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pulse recording from detector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Connecting to ZedBoard via UDMA\n",
    "\n",
    "> Connect to the ZedBoard at the designated IP address and port"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the UDMA instance and then connect to the ZedBoard. Assign it to the variable called \"zedBoard\"\n",
    "zedBoard = udma.UDMA_CLASS( ......"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Do not forget to disable the logging in UDMA to speed-up the transactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Disable unused logging to speed up data transactions\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setting up pulse acquisition parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trigger logic block in the Vivado Block design acts as a simple oscilloscope. Set the parameter values that you'll use for the acquisition. These values can be changed according to your needs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "THRESHOLD_LEVEL = DC_OFFSET + 1000  # In ADC values (must ALWAYS include the DC offset)\n",
    "SAMPLES_BEFORE_TRIGGER = 512        # Samples to be recorded before the trigger\n",
    "TOTAL_PULSES_TO_RECORD = 100        # Number of pulses to be recorded"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following registers must match your Vivado design\n",
    "\n",
    "Comblock Register Outputs -> TriggerLogic Block Inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Write the ComBlock register outputs that match the TriggerLogic block inputs, according to your block design in Vivado\n",
    "\n",
    "Use the constants from in the previous cell, defined with self-explanatory names:\n",
    "- `THRESHOLD_LEVEL`\n",
    "- `SAMPLES_BEFORE_TRIGGER`\n",
    "- `TRACE_LEN`\n",
    "\n",
    "🔴 Notice that `THRESHOLD_LEVEL` is defined with a DC_OFFSET that must be always present 🔴"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the constants defined in the previous code block (THRESHOLD_LEVEL, SAMPLES_BEFORE_TRIGGER, TRACE_LEN)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Initializing pulse recording and fitting library"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setting up recording and analysis parameters\n",
    "pulseAnalysis = PulseFit(\n",
    "    pulseLenSamples = TRACE_LEN,\n",
    "    samplingRate = SAMPLING_RATE,\n",
    "    resolutionBits = ADC_RESOLUTION\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Recording dataset\n",
    "The ```recordDataset``` method is a high-level function that executes several times (```TOTAL_PULSES_TO_RECORD```) the ```UDMA.read_fifo``` instruction. You are already familiar with this UDMA functionality from Lab 4, so we provide you a shortcut here.\n",
    "\n",
    "Results will be recorded in the variable ```dataset```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = pulseAnalysis.recordDataset(\n",
    "    pulseCount = TOTAL_PULSES_TO_RECORD,\n",
    "    udmaInstance = zedBoard\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After several seconds, you can observe the recorded dataset into a dataframe.\n",
    "\n",
    "Each row represents a recorded pulse, while each column is the sampled analog value from the ADC.\n",
    "\n",
    "Consequently, the number of columns must match with the ```TRACE_LEN``` constant, and the number of rows must be equal to the ```TOTAL_PULSES_TO_RECORD``` value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Just printing the recorded dataset values\n",
    "dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A visual representation of the recorded dataset, previous to the cleaning process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timeAxisMicroseconds = pulseAnalysis.getTimeAxisContinuous(useMicroSeconds = True)\n",
    "timeAxisSeconds = pulseAnalysis.getTimeAxisContinuous(useMicroSeconds = False)\n",
    "PLOT_Y_OFFSET = 256\n",
    "plt.figure()\n",
    "plt.plot(timeAxisMicroseconds, dataset.T.values)\n",
    "plt.title(\"Raw recorded dataset\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## B) Data analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### B.1) Data wrangling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Removing saturated pulses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following steps will take the original dataset and remove the saturated pulses "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datasetNoSat = pulseAnalysis.removeSaturated(dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting the recorded dataset along the non-saturated dataset\n",
    "\n",
    "plt.figure(figsize=(12,5))\n",
    "plt.subplot(1, 2, 1)\n",
    "plt.plot(timeAxisMicroseconds, dataset.T.values)\n",
    "plt.title(\"Original recorded dataset\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.subplot(1, 2, 2)\n",
    "plt.title(\"Non-saturated pulses\")\n",
    "plt.plot(timeAxisMicroseconds, datasetNoSat.T.values)\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Restoring baseline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the dataset without saturated events (`datasetNoSat`) to restore their baseline average to `0` adc units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datasetNoSatNoBl = pulseAnalysis.restoreBaseline(datasetNoSat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12,5))\n",
    "\n",
    "plt.subplot(1, 2, 1)\n",
    "plt.title(\"Pulses before baseline restoring\")\n",
    "plt.plot(timeAxisMicroseconds, datasetNoSat.T.values)\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.subplot(1, 2, 2)\n",
    "plt.plot(timeAxisMicroseconds, datasetNoSatNoBl.T.values)\n",
    "plt.title(\"Pulses with restored baseline\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-200, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Removing small pulses\n",
    "\n",
    "This step is required to use the pulses that were recorded with the highest amplitude definition, leading to a lower fitting error.\n",
    "\n",
    "Notice that the amount of pulses may be considerably reduced after this filtering process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the minimum amplitude of the pulses to be used in the fitting process.\n",
    "# Use the previous plot to inspect a reasonable limit.\n",
    "amplitudeThreshold = 2048"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datasetFinal = pulseAnalysis.filterPulsesByAmplitude(datasetNoSatNoBl, threshold = amplitudeThreshold)\n",
    "print(f\"Final dataset pulse count: {len(datasetFinal)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Final dataset\n",
    "Notice the difference of the original dataset, compared with the final version of it.\n",
    "The following steps were carried out:\n",
    "- Removed saturated pulses: their contribution to energy estimation is null, so they are removed\n",
    "- Restored baseline: the average signal value before the pulse remains close to 0, so the pulse height can be reliably be determined\n",
    "- Removed low-amplitude pulses: aiming to maximize the amplitude granularity of the pulses that will be further analyzed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(12,5))\n",
    "plt.subplot(1, 2, 1)\n",
    "plt.plot(timeAxisMicroseconds, dataset.T.values)\n",
    "plt.title(\"Original recorded dataset\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.subplot(1, 2, 2)\n",
    "plt.plot(timeAxisMicroseconds, datasetFinal.T.values)\n",
    "plt.title(\"Final (clean) dataset\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.hlines(y = amplitudeThreshold, xmin = timeAxisMicroseconds[0], xmax = timeAxisMicroseconds[-1], linestyles='--', colors = 'gray')\n",
    "plt.ylim((-PLOT_Y_OFFSET, 2**ADC_RESOLUTION + PLOT_Y_OFFSET))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### B.2) Pulse fitting - detector characterization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By visually inspecting the last plot (pulses from **final dataset**), define the estimated parameters (initial guess) for the fitting process. Carefully notice the time units in the plot: the parameters must be defined in **seconds**.\n",
    "\n",
    "To define a ```variable``` with the 3.7x10^-9 value in Python, you may use the following format for scientific notation: \n",
    "\n",
    "```python\n",
    "variable = 3.7e-9\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Define the estimated decay and rise time constants of the detector, as well as the time of arrrival of the pulses"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the initial guesses, using second units (not microseconds)\n",
    "\n",
    "estimatedTauD = 5e-6 # Define your own estimation of decay constant using visual inspection\n",
    "estimatedTauR = 1e-6 # Set your estimation for pulse rise time constant using visual inspection\n",
    "estimatedT0 = 5e-6 # Set the estimated time of arrival from the visual inspection of the plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Execute the fitting process next"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fitParameters, fitRelativeErrors = pulseAnalysis.fitPulses(dataset = datasetFinal,\n",
    "                                                   estimatedTauD = estimatedTauD,\n",
    "                                                   estimatedTauR = estimatedTauR,\n",
    "                                                   estimatedT0 = estimatedT0\n",
    "                                                   )\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Fitting results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These values represent the unique parameters of your detector. **Write them down**, since they will be useful in the next stage of the project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Printing out the parameter fitting results\n",
    "fitParameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Relative errors are useful to determine whether the fitting was successful or not. If these values are too high (e.g. larger than **0.05**, or **5%**), you may repeat the fitting process with other initial guesses.\n",
    "\n",
    "In case either `inf` or `-inf` values appear, your initial estimations were too far away for the experimental data used for fitting. Please repeat the fitting process by carefully setting the initial parameters (`estimatedTauD`, `estimatedTauR`, `estimatedT0`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Printing out the parameter fitting relative errors\n",
    "fitRelativeErrors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Visual inspection of fitting result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the \"ideal\" pulse shape using the fitted parameters, along with an experimental pulse you have just recorded: they should visually match. \n",
    "\n",
    "This is another way to easily get a hint of the fitting results. Notice that experimental data will always contain noise that may be visible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using the 'fitParameters' that contains the fitting results\n",
    "idealPulse = pulseAnalysis.pulseModel(t = timeAxisSeconds,\n",
    "                                      A = 1,\n",
    "                                      tauD = fitParameters['fit_tauDecay'],\n",
    "                                      tauR = fitParameters['fit_tauRise'],\n",
    "                                      t0 = fitParameters['fit_tArrival_t0'])\n",
    "\n",
    "# Normalizing amplitude\n",
    "idealPulse /= np.max(idealPulse)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choosing a random pulse from the recorded dataset\n",
    "experimentalPulse = datasetFinal.sample().T.values\n",
    "\n",
    "# Normalizing amplitude\n",
    "experimentalPulse = experimentalPulse/np.max(experimentalPulse)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(timeAxisMicroseconds, idealPulse, '-', linewidth = 3)\n",
    "plt.plot(timeAxisMicroseconds, experimentalPulse, '--')\n",
    "plt.title(\"Pulse shape comparison\")\n",
    "plt.xlabel(\"Time ($\\mu s$)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.legend([\"Fitted (ideal) pulse\", \"Experimental data\"])\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
