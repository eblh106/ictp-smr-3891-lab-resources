{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 4 - SoC-FPGA DAQ system and TCL\n",
    "This Jupyter Notebook (JN) is the continuation of the Lab guide that will take you through the necessary steps to interact with the ZedBoard development board via Ethernet using the UDMA framework. All the provided source code is written in Python 3.\n",
    "\n",
    "In this Lab exercise you will not only interact with the design in the Zynq SoC, but also with the external DAC and ADC boards plugged into the ZedBoard PMOD connectors. To do so, we have prepared an environment based on Python 3.8 and the required libraries to execute the workflow described next:\n",
    "\n",
    "1) **Python**\n",
    "  - Synthesize periodic signals (waveforms) as individual samples, represented by a numeric array \n",
    "2) **Python + UDMA <---> Zynq SoC**\n",
    "  - Send from this JN to the ComBlock's DP-RAM the waveform individual samples that represent the DAC output\n",
    "  - Use the UDMA library to write individual ComBlock output registers to setup the ADC and DAC driver values\n",
    "  - Use the UDMA library to read the contents of the data stored in the ComBlock's FIFO\n",
    "3) **Python**\n",
    "  - Assess the contents of the data read from the ComBlock's FIFO with the expected behavior of the FIR filter within the programmable logic design in the SoC\n",
    "\n",
    "All the required steps are documented in this JN, so you can easily follow them along with the code you execute in the notebook blocks.\n",
    "\n",
    "---\n",
    "\n",
    "- Author(s): Ivan Morales (MLab/ICTP) - 2023/10/31 (version 1.0)\n",
    "- Update(s):\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing libraries\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setting up plot rendering handler for Jupyter Notebook\n",
    "%matplotlib widget\n",
    "\n",
    "# == MLab libraries ==\n",
    "import udma #MLab UDMA library\n",
    "from waveforms import Waveforms #MLab waveform synthesis library\n",
    "\n",
    "# Other libraries\n",
    "import numpy as np\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1) Python signal synthesis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Synthetic waveforms library\n",
    "\n",
    "Using the 'waveforms' library to generate the stimuli sent to the hardware platform (ZedBoard).\n",
    "\n",
    "The generated data in this step will be further sent to be stored in ComBlock DP-RAM and automatically fetched out through the DAC (PMOD-DA1) analog output.\n",
    "\n",
    "Both the ADC and DAC operate at 1 MHz, leading to a Nyquist theoretical limit of 500 kHz. Since no antialiasing nor reconstruction filters are used, the practical frequency limit for the waveforms is considerably reduced: we recommend you not to exceed **50 kHz** to get a _recognizable_ signal shape.\n",
    "\n",
    "The library provides four types of periodic waveforms using the ```computeWaveform``` method:\n",
    "- Sinusoidal\n",
    "- Sawtooth\n",
    "- Square\n",
    "- Noise (white Gaussian)\n",
    "\n",
    "Also, a _chirp_-like waveform can be generated using the ```computeSweep``` method. A sinusoidal signal with a frequency sweep is generated, according to the provided frequency limits as parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'''\n",
    " We define the global signal synthesis parameters here:\n",
    " do not change them if not sure about their behavior.\n",
    "''' \n",
    "\n",
    "# Total trace length (samples). This value must match the Vivado Block Design settings.\n",
    "TRACE_LEN = 4096\n",
    "\n",
    "# ADC and DAC sampling rate\n",
    "SAMPLING_RATE = 1e6 # 1e6 = 1x10^6 = 1 MHz\n",
    "\n",
    "# Data must be split to be sent in chunks/packets via UDMA to avoid TCP segmentation\n",
    "TRACE_SEGMENTS = 32 # Do not change this value\n",
    "CHUNK_SIZE = TRACE_LEN//TRACE_SEGMENTS # Must be always smaller than 250 elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initializing the waveform synthesis library\n",
    "\n",
    "We are creating an instance of the **Waveforms** library. The initialization is carried out only once.\n",
    "\n",
    "The instance name is ```wf```: this is the element you will be interacting with to generate the waveforms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Creating the waveforms library instance\n",
    "\n",
    "wf = Waveforms(length = TRACE_LEN, # Number of samples of each waveform\n",
    "               samplingFreq = SAMPLING_RATE, # Sampling frequency of the ADC and DAQ \n",
    "               dacBits = 8)        # Resolution (bits) of the DAC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Waveform synthesis\n",
    "\n",
    "Here you will synthesize multiple waveforms and plot them to easily identify their properties.\n",
    "\n",
    "Please, **do not** exceed the Nyquist limit (500 kHz) in any case.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Periodic waveform parameters\n",
    "The waveform synthesis library is capable of generating periodic signals, according to the provided parameters.\n",
    "\n",
    "To generate a periodic waveform, the ```computeWaveform``` method is used. If you not provide any parameter, the following waveform will be generated by default:\n",
    "- Waveform: 'sine'\n",
    "- Frequency: 10 kHz\n",
    "- Amplitude: 250 (DAC step units)\n",
    "\n",
    "Since the DAC resolution is 8 bits, the maximum supported amplitude value is **255** ($2^8 - 1$).\n",
    "\n",
    "As a starting point, we provide to you two waveforms:\n",
    "- A 20 kHz sinewave with amplitude 200\n",
    "- A _frequency-sweep_ sinewave with a frequency range from 5 kHz to 50 kHz and constant amplitude 250."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### List of available periodic waveforms\n",
    "Executing the next code block will generate a list of the available waveforms in the library when using the ```computeWaveform``` method. Use **only** these names to generate the periodic waveforms that will be requested in the further steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wf.getListOfWaveforms()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 20 kHz sinewave"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This waveform is synthesized with a frequency _close_ to 20 kHz, but not exactly: the actual frequency value es 19531 Hz. Can you imagine why this _non-round_ value was chosen? Let the instructors know any thoughts about this particular frequency.\n",
    "\n",
    "__Hint__: You're about to store this array of samples (waveform) in a 4096-element RAM that will emulate an arbitrary waveform generator. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting waveform is stored in the variable ```sineWave20k``` as an array (list) of 4096 integer elements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ~20 kHz sine waveform\n",
    "sineWave20k = wf.computeWaveform(waveformName = 'sine', \n",
    "                              frequencyHz = 19531,  # Why not 20e3 exactly?\n",
    "                              amplitude = 200)\n",
    "\n",
    "fig = plt.figure()\n",
    "plt.plot(sineWave20k)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Frequency sweep in sinusoidal waveform\n",
    "A sinusoidal waveform with a frequency sweep is generated. In this example, the range of frequencies are from 1 kHz to 50 kHz. \n",
    "\n",
    "The resulting waveform is stored in the variable ```sweep1k50k``` as an array (list) of 4096 integer elements.\n",
    "\n",
    "The ```computeSweep``` method is used to synthesize this type of waveform."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sinusoidal sweep from 1 kHz to 50 kHz\n",
    "sweep1k50k = wf.computeSweep(freqStart = 1e3, freqEnd = 50e3, amplitude = 250)\n",
    "\n",
    "fig = plt.figure()\n",
    "plt.plot(sweep1k50k)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create your own waveforms\n",
    "\n",
    "Now it's time for you to create the new waveforms using the provided library. Create a variable with the requested description in the following blocks. Keep using the same naming convention: for instance, a 12 kHz square waveform can be associated with the name ```sineWave12k```."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 5 kHz sinewave\n",
    "Create a variable called ```sineWave5k``` containing a 5 kHz sine wave with the maximum DAC amplitude. Plot it in the same code block, so it can be easily inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 5 kHz sine waveform\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 10 kHz sawtooth waveform\n",
    "\n",
    "Create a variable called ```sawtoothWave10k``` assigned to a 10 kHz sawtooth waveform and 200 amplitude units. Plot it in the same code block, so it can be easily inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 10 kHz sawtooth waveform\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 16384 Hz square waveform\n",
    "\n",
    "Create a variable called ```sawtoothWave16k``` assigned to a 16384 Hz square waveform and 128 amplitude units. Plot it in the same code block, so it can be easily inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 16384 Hz square waveform\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Additive white Gaussian noise (AWGN) with $\\sigma = 60$\n",
    "\n",
    "Create a variable called ```awgnSigma60``` to represent white noise with an std = 60 units. Due to the nature of the white noise, no frequency range or limit parameters are required. Plot the waveform in the same code block, so it can be easily inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Additive white Gaussian noise with sigma = 60\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Summary of generated waveforms\n",
    "\n",
    "At this point, you should be able to use the waveforms described in the following table\n",
    "\n",
    "**Table 1  - Synthesized waveforms**\n",
    "| Variable Name     | Description |\n",
    "| ----------- | ----------- |\n",
    "| ```sineWave20k```     | ~20 kHz sinusoidal waveform |\n",
    "| ```sweep1k50k```      | Sinusoidal frequency sweep 1 kHz -> 50 kHz|\n",
    "| ```sineWave5k```      | 5 kHz sinusoidal waveform |\n",
    "| ```sawtoothWave10k``` | 10 kHz sawtooth waveform |\n",
    "| ```squareWave16k```   | 16384 Hz square waveform |\n",
    "| ```awgnSigma60```     | Additive white Gaussian noise with $\\sigma = 60$ |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2) Interfacing with hardware via UDMA\n",
    "In this section you will make use of the synthesized waveforms from the last section to periodically execute the following loop:\n",
    "\n",
    "- Transmit a synthesized waveform to the ComBlock's RAM using the UDMA.\n",
    "- The hardware design in the PL of the SoC will continuously send these samples to the DAC (plugged into the ZedBoard's PMOD connector), so they get translated to the analog domain.\n",
    "- This analog signal will be continuously sampled by the ADC (also plugged into the ZedBoard PMOD connector).\n",
    "- The sampled values from the ADC stream will feed the input of the finite impulse response (FIR) filter that was generated with High-Level Synthesis (HLS).\n",
    "- The output of the FIR filter will be stored in the ComBlock's input FIFO.\n",
    "- The data stored in the FIFO will be sent from the SoC to this computer using UDMA."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up UDMA and ZedBoard parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "IP Address and port of your ZedBoard development board"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "IP_ADDRESS = 'xxx.xxx.xxx.xxx' # Set your ZedBoard IP Address here\n",
    "IP_PORT = 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initializing UDMA class instance with provided IP settings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zedBoard = udma.UDMA_CLASS(IP_ADDRESS, IP_PORT)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connecting to ZedBoard"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tries indefinitely until connection is successful"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connectionStatus = 0\n",
    "while(connectionStatus == 0):\n",
    "    connectionStatus = zedBoard.connect()\n",
    "    time.sleep(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Disable unused logging to speed up data transactions\n",
    "_ = zedBoard.log(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with ZedBoard via UDMA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transmit waveform to ComBlock's DP-RAM"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Transmitting to ComBlock's DP-RAM the waveform array (vector). This step only performed once: the hardware design in the PL will take care to indefinitely loop through the waveform samples and push the them out to the DAC.\n",
    "\n",
    "Feel free to exchange the waveform variable, choosing from those ones you synthesized in the previous section.\n",
    "\n",
    "🔴 The next code block is where you **set the waveform** to be looped in the hardware 🔴"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Writing to ComBlock's RAM one of the provided waveforms (detailed in Table 1)\n",
    "\n",
    "zedBoard.write_ram(addr = 0,\n",
    "                   offset = 0,\n",
    "                   length = TRACE_LEN,\n",
    "                   inc = 1,\n",
    "                   TxData = sineWave20k) # <--- This is the waveform variable that will be sent\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Verifying RAM contents\n",
    "The waveform that was sent in the previous step must be already present in the ComBlock's DP-RAM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "waveformStoredInRam = zedBoard.read_ram(0, TRACE_LEN, 1)[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the contents read from the ComBlock's DP-RAM"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "plt.title(\"Waveform stored in ComBlock's DP-RAM\")\n",
    "plt.xlabel(\"Time (samples)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.plot(waveformStoredInRam, '-', markersize = 1)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sampling analog signal and processing using the FIR filter\n",
    "Dataflow (in SoC PL) to sample the data:\n",
    "- Data are sampled from the ADC and sent to an AXI-Stream interface to the FIR filter\n",
    "- Digitized data are processed in FIR filter\n",
    "- Processed data captured in Comblock's input FIFO\n",
    "- Data from ComBlock's FIFO are fetched via UDMA to this Jupyter Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setting up the configuration bits of the DAC to enable output\n",
    "zedBoard.write_reg(2, 0x20) # Set DAC control bits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Enabling writing and reading to/from DAC+ADC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zedBoard.write_reg(0, 0x01) # Enable ADC operation\n",
    "zedBoard.write_reg(1, 0x01) # Enable DAC operation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function definition: used to clear out iFIFO contents prior to read any value\n",
    "def clearInFifo(udmaInstance):\n",
    "    udmaInstance.write_reg(17, 1)\n",
    "    udmaInstance.write_reg(17, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Acquisition from Comblock's FIFO using UDMA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you will fetch the latest data from the FIR filter output using DMA. Execute this block as many times as you want (```Ctrl+Enter```) to observe the results in the time domain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Clear input FIFO contents to get the latest data from the FIR filter output\n",
    "clearInFifo(zedBoard)\n",
    "\n",
    "# Empty list declaration: the data read from the ComBlock's FIFO will be stored here\n",
    "fifoInputTrace = []\n",
    "\n",
    "# Fetching data from ComBlock's FIFO in chunks to avoid TCP segmentation\n",
    "for i in range(TRACE_SEGMENTS):\n",
    "    fifoSamples = zedBoard.read_fifo(CHUNK_SIZE)[1]\n",
    "    \n",
    "    # Matching the numerical representation of the FIR filter output (integer 16 bits)\n",
    "    fifoSamples = np.array(fifoSamples).astype(np.int16)\n",
    "    \n",
    "    # Appending to the result array the latest data read from FIFO\n",
    "    fifoInputTrace.extend(fifoSamples)\n",
    "\n",
    "\n",
    "# Plotting the acquired trace (FIR output)\n",
    "fig = plt.figure()\n",
    "plt.plot(fifoInputTrace)\n",
    "plt.title(\"FIR filter output\")\n",
    "plt.xlabel(\"Time (samples)\")\n",
    "plt.ylabel(\"Amplitude (ADC units)\")\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3) Data analysis with Python\n",
    "In this section you will be introduced to a simple analysis for the captured traces, resulting from the FIR filter output data.\n",
    "\n",
    "The Fast Fourier Transform (FFT) algorithm will be used to translate the sampled data into the frequency domain. This will allow you to easily visualize the response of the filter to the input waveform used as the stimulus (synthesized waveform). \n",
    "\n",
    "The power spectral density (PSD) plot will be created, representing the power density contents of the retrieved signal for each frequency component. The sampling frequency is constant ($f_s$ = 1 MHz), allowing for an easy translation of the analog frequency representation in the PSD plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Power spectral density (PSD) of the filter output\n",
    "\n",
    "Observe how the filter output changes when testing with different the stimuli (synthesized waveforms). \n",
    "- Test different waveforms as stimuli of the FIR filter using the UDMA code in section 2\n",
    "  - Notice that even with the filter, harmonics will appear for sawtooth and square waveforms\n",
    "  - Although the noise trace should be much longer to emulate a plain spectrum, observe the behavior after the FIR output\n",
    "- Test the frequency sweep as the stimulus to assess the frequency response of the FIR filter\n",
    "  - Compare this result to the [frequency parameters of the filter](https://gitlab.com/ictp-mlab/smr-3891/-/wikis/Labs/Lab-4-ADC-DAC-loopback-and-TCL#131-hls-based-fir-ip-core) exposed in the Lab Guide:\n",
    "    - Frequency ranges\n",
    "    - Gain at pass and rejection bands\n",
    "    - Ripple at pass band\n",
    "\n",
    "\n",
    "You can also play around with composite waveforms: you may take two or more signals and sum them up or even multiply them (AM modulation). Just be careful to normalize the outputs to the maximum DAC output value ($2^8 - 1$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.fft\n",
    "\n",
    "# Creating the x-axis of the plot (frequency axis in kHz scale)\n",
    "fftLen = TRACE_LEN//2\n",
    "xAxis = np.linspace(0, (SAMPLING_RATE//2)/1000, fftLen)\n",
    "\n",
    "# Computing the FFT magnitude to obtain the PSD (in dB/Hz)\n",
    "psdMagnitude = np.abs(scipy.fft.fft(fifoInputTrace))[:fftLen]\n",
    "psdMagnitude = 10*np.log(psdMagnitude)\n",
    "\n",
    "# Plotting the filter output in the frequency domain\n",
    "fig = plt.figure()\n",
    "plt.plot(xAxis, psdMagnitude)\n",
    "\n",
    "plt.title(\"Power spectral density output of the FIR filter (HLS)\")\n",
    "plt.xlabel('Frequency (kHz)')\n",
    "plt.ylabel('Power spectral density (dB/Hz)')\n",
    "plt.xscale('log')\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4) Challenge"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have had the chance to verify how to perform the UDMA interaction with the hardware, two challenge exercises are proposed, sorted in increasing difficulty.\n",
    "\n",
    "Refer to the [Lab Guide](https://gitlab.com/ictp-mlab/smr-3891/-/wikis/Labs/Lab-4-ADC-DAC-loopback-and-TCL#6-challenge) for the detailed requirements:\n",
    "- Challenge 1 - Create the [bode magnitude plot](https://en.wikipedia.org/wiki/Bode_plot) of the filter response\n",
    "  - Hint: synthesize multiple sine waveforms with same amplitude and different frequencies\n",
    "- Challenge 2 - Modify the FIR filter parameters to achieve different frequency response using the High-level synthesis source files\n",
    "  - Hint: you may use the [TFilter tool](http://t-filter.engineerjs.com/) to generate the coefficients\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
