#python == 3.8.10
numpy == 1.19.5
scipy == 1.6.2
matplotlib == 3.4.0
ipympl == 0.9.3
