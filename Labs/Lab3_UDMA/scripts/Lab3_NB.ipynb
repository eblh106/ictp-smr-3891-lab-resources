{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 3 - UDMA\n",
    "\n",
    "This Jupyter Notebook (JN) is the final part of Lab3. This will show you how to use the UDMA from a JupyterLab notebook to interface your custom hardware through the ComBlock.\n",
    "\n",
    "In this Lab exercise you will not only interact with the design in the Zynq SoC, but also with the external DAC and ADC boards plugged into the ZedBoard PMOD connectors. To do so, we have prepared an environment based on Python 3.8 and the required libraries to execute the workflow described next:\n",
    "\n",
    "1) **Python**\n",
    "  - Instantiate a UDMA object\n",
    "  - Connect the UDMA object to the Zedboard\n",
    "2) **Python + UDMA <---> Zynq SoC**\n",
    "  - Complete the missing parts in the code denoted by ?? signs\n",
    "  - Test the ALU\n",
    "  - Test the Flopoco division block\n",
    "  - Test the HLS division block\n",
    "3) **Python**\n",
    "  - Perform the appropriate casting to the floating point values returned by the division blocks\n",
    "\n",
    "- Author(s): Werner Florian, Agustin Silva (MLab/ICTP) - 2023/10/31 (version 1.0)\n",
    "- Update(s):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing libraries\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# == MLab library ==\n",
    "import udma #MLab UDMA library\n",
    "\n",
    "# Bit manipulation library\n",
    "from struct import pack, unpack\n",
    "\n",
    "from time import sleep"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interfacing with hardware via UDMA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up UDMA and ZedBoard parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "IP Address and port of your ZedBoard development board"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "IP_ADDRESS = 'XXX.XXX.XXX.XXX' # Set your ZedBoard IP Address here\n",
    "IP_PORT = 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initializing UDMA class instance with provided IP settings. Here we are creating a `UDMA` object and giving it the `zedBoard` name. This name can be chosen at random."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zedBoard = udma.UDMA_CLASS(IP_ADDRESS, IP_PORT)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connecting to ZedBoard"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tries indefinitely until connection is successful"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connectionStatus = 0\n",
    "while(connectionStatus == 0):\n",
    "    connectionStatus = zedBoard.connect()\n",
    "    sleep(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Disable unused logging to speed up data transactions, logging capabilities are provided via UART and can be turn on or off on command. Check the **GTKterm** window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = zedBoard.log(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with ZedBoard via UDMA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write values to the registers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start we will try writing and reading the registers of the **ComBlock**.\n",
    "\n",
    "The following register map will be useful when trying to remember which register was connected to which port.\n",
    "\n",
    "![reg_map](img/reg_map.png)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Output registers\n",
    "CB_DATA_A_REG\t\t    = 0 # variable data a\n",
    "CB_DATA_B_REG\t\t    = 1 # variable data b\n",
    "CB_OP_CODE_REG\t        = 2 # operation code register\n",
    "CB_DIVHLS_A_REG\t\t    = 3 # variable data a\n",
    "CB_DIVHLS_B_REG\t\t    = 4 # variable data b\n",
    "\n",
    "# Input registers\n",
    "CB_ALU_STATUS_REG\t    = 0 # ALU status flag\n",
    "CB_ALU_DATA_O_REG\t    = 1 # ALU data operation result\n",
    "CB_DIV_EXN_REG\t\t    = 2 # Division exn flag\n",
    "CB_DIV_DATA_O_REG\t    = 3 # Division data operation result\n",
    "CB_DIVHLS_DATA_O_REG    = 4 # Read slide switches"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start we invoke our `UDMA` object, the `zedBoard`. This object provides the following functions to interact with the registers:\n",
    "\n",
    "`.write_reg(int32 reg, int32 data) -> list [head, 0]`\n",
    "\n",
    "`.read_reg(int32 reg) -> [head, rx_dat]`\n",
    "\n",
    "Both functions return a list which contains two entries, `head` and the result of the operation.\n",
    "\n",
    "```\n",
    "head [0]: error report\n",
    "\t0 -> Failure\n",
    "\t1 -> Success\n",
    "\t2 -> overflow\n",
    "\t3 -> underflow\n",
    "\t4 -> logging enabled\n",
    "\t5 -> logging disabled\n",
    "head [1]: length\n",
    "```\n",
    "\n",
    "Using this information you should now proceed to replicate the tests from [Lab 2: ComBlock and RTL](https://gitlab.com/ictp-mlab/smr-3891/-/wikis/Labs/Lab-2-ComBlock-and-RTL#51-slide-switches-interaction-with-the-comblock-ip)\n",
    "\n",
    "You should replicate the following code for all the ALU operations and both division blocks. As in the previous laboratory the results from the division blocks makes little to no sense. This is the subject of the next section.\n",
    "\n",
    "To read only the data from the registers you can access the item in the tuples adding `[1][0]` the fist square brackets extract the `rx_dat` and the second takes the first element.\n",
    "\n",
    "`.read_reg(int32 reg)[1][0]`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"### Testing the ALU\")\n",
    "\n",
    "data_a = 10\n",
    "data_b = 5\n",
    "\n",
    "# write the operands and op_code\n",
    "zedBoard.write_reg(reg = ??, data = ??)\n",
    "zedBoard.write_reg(reg = ??, data = ??)\n",
    "\n",
    "print(f\"Data in A = {data_a}, B = {data_b}\")\n",
    "\n",
    "for i in range(8):\n",
    "    zedBoard.write_reg(reg = CB_OP_CODE_REG, data = i)\n",
    "    # read the head and the result\n",
    "    status = zedBoard.read_reg(reg = ??)[1][0]\n",
    "    print(f\"operation status: {status}\")\n",
    "    data_out = zedBoard.read_reg(reg = ??)[1][0]\n",
    "    # show the result\n",
    "    print(f\"operation option = {i}, result = {data_out}\")\n",
    "\n",
    "print(\"\\n### Testing the Flopoco division block\")\n",
    "exn = zedBoard.read_reg(reg = ??)[1][0]\n",
    "print(f\"operation exn: {exn}\")\n",
    "\n",
    "data_out = zedBoard.read_reg(reg = ??)[1][0]\n",
    "print(f\"division R: {data_out}\\n\")\n",
    "\n",
    "print(\"### Testing the HLS division block\")\n",
    "zedBoard.write_reg(reg = ??, data = data_a)\n",
    "zedBoard.write_reg(reg = CB_DIVHLS_B_REG, data = ??)\n",
    "data_out_hls = zedBoard.read_reg(reg = ??)[1][0]\n",
    "print(f\"division R: {data_out_hls}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Floating point representation\n",
    "\n",
    "Floating point is radically different from other representations in the way that it encodes quantities.\n",
    "In a similar way to scientific notation, floating point quantities are encoded in an integer value called *significand* that is scaled in the *base* (b) with an *exponent* (e) and *precision* of the mantissa (p), shown in the following equation:\n",
    "\n",
    "![floating](img/fp.png)\n",
    "\n",
    "The *significand* can also be simplified when to the *mantissa* can represent the significant digits of the fraction, in this case the precision must include the implicit extra digit.\n",
    "\n",
    "The standard used by the division blocks is the IEEE 754, where *b = 2* and *p = 24*.\n",
    "It prescribes a 32 bit word that is split as follows, the *fraction* or *mantissa* denotes that one unit is inferred as an extra digit making the resulting *significand = 1.fraction* in the appropriate base.\n",
    "This extra digit is why the precision is actually 24:\n",
    "\n",
    "![standard](img/ieee754.png)\n",
    "\n",
    "The *significand* from a binary fraction is defined as:\n",
    "\n",
    "$$ significand = 1 + \\frac{mantissa}{2^{p-1}} $$\n",
    "\n",
    "Now we will proceed to cast the 32 bit word from the division blocks to IEEE 754.\n",
    "This will make the values usable in Python as common floating point.\n",
    "\n",
    "$$ floating\\_point = (-1)^{sign} * significand * 2^{exponent - 127}$$\n",
    "\n",
    "The next part consists in writing the floating point values in the correct representation in the **ComBlock** and casting the output values form the division blocks to floating point."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def raw_2_float(raw_val: int):\n",
    "    ?? = raw_val >> 31\n",
    "    ?? = (raw_val >> 23) & 0xFF\n",
    "    ?? = raw_val & 0x7FFFFF\n",
    "    return sign, exponent, mantissa\n",
    "\n",
    "def build_ieee754(sign: int, exponent: int, mantissa: int):\n",
    "    return (-1)**(??) * (1 + ??/(2**(24-1))) * 2**(?? - 127)\n",
    "\n",
    "sign, exponent, mantissa = raw_2_float(data_out)\n",
    "print(sign, exponent, mantissa)\n",
    "\n",
    "float_data_out = build_ieee754(sign, exponent, mantissa)\n",
    "print(f\"Float IEEE 754 representation of our raw data {float_data_out:.30f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results makes little to no sense given that we all know that $10 / 5 \\neq 1.000000596046447753906250000000$. \n",
    "And in the case of the HLS we obtain a value that is equivalent to `NaN` according to the IEEE 754 standard.\n",
    "This signals another error in the way we are using this blocks so far.\n",
    "The output is considered floating point and casted correctly, but what is missing is casting the inputs to the correct floating point format for the correct interpretation inside the IPs.\n",
    "\n",
    "We can cast our floating point values to their equivalent representation in integer by doing the previous steps in an inverse way.\n",
    "\n",
    "1. Obtain the *sign*, *exponent*, and *mantissa* bits.\n",
    "2. Build the integer representation with the correct masks and shifts.\n",
    "3. Profit!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def float_2_raw(float_val: float):\n",
    "    float_val_hex = float_val.hex()\n",
    "    sign = float_val < 0\n",
    "    exponent = int(float_val_hex.partition('p')[2]) + 127\n",
    "    mantissa = int(float_val_hex[float_val_hex.find('.') + 1:float_val_hex.find('p')], 16) >> 29\n",
    "    return sign, exponent, mantissa\n",
    "\n",
    "def build_int(sign, exponent, mantissa):\n",
    "    return (?? << 31) + (?? << 23) + ??\n",
    "\n",
    "sign, exponent, mantissa = float_2_raw(10.1)\n",
    "print(sign, exponent, mantissa)\n",
    "\n",
    "int_data_out = build_int(sign, exponent, mantissa)\n",
    "print(f\"Integer representation of our Float IEEE 754 {int_data_out:x}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have all the tools you need to write floating point values to your blocks and re-interpret the result into floating point, proceed to test them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"### Testing the division blocks\")\n",
    "\n",
    "fp_data_a = 10.0\n",
    "fp_data_b = 5.0\n",
    "\n",
    "# Use the previous functions to translate the floating point IEEE 754 to raw bits in the following space\n",
    "\n",
    "\n",
    "# Write the operands to the appropriate blocks inputs\n",
    "\n",
    "print(\"\\n### Testing the Flopoco division block\")\n",
    "\n",
    "zedBoard.write_reg(reg = CB_DATA_A_REG, data = ??)\n",
    "zedBoard.write_reg(reg = CB_DATA_B_REG, data = ??)\n",
    "\n",
    "exn = zedBoard.read_reg(reg = CB_DIV_EXN_REG)[1][0]\n",
    "print(f\"operation exn: {exn}\")\n",
    "\n",
    "data_out = zedBoard.read_reg(reg = CB_DIV_DATA_O_REG)[1][0]\n",
    "print(f\"Raw division R: {data_out}\")\n",
    "\n",
    "# Convert from the raw bits from the UDMA to floating point using the previous functions and save it in a variable named float_data_out\n",
    "#...\n",
    "#...\n",
    "\n",
    "print(f\"Float division R: {float_data_out}\\n\")\n",
    "\n",
    "print(\"### Testing the HLS division block\")\n",
    "\n",
    "zedBoard.write_reg(reg = CB_DIVHLS_A_REG, data = ??)\n",
    "zedBoard.write_reg(reg = CB_DIVHLS_B_REG, data = ??)\n",
    "\n",
    "data_out_hls = zedBoard.read_reg(reg = CB_DIVHLS_DATA_O_REG)[1][0]\n",
    "print(f\"Raw division R: {data_out_hls}\")\n",
    "\n",
    "# Convert from the raw bits from the UDMA to floating point using the previous functions and save it in a variable named float_data_out_hls\n",
    "#...\n",
    "#...\n",
    "\n",
    "print(f\"Float division R: {float_data_out_hls}\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are some functions that will prove useful in this endeavour:\n",
    "\n",
    "```pack(format, v)```\n",
    "\n",
    "Return a bytes object containing the values `v`, packed according to the format string `format`.\n",
    "\n",
    "```unpack(format, buffer)```\n",
    "\n",
    "Unpack from the buffer `buffer` according to the format string `format`. The result is a tuple even if it contains exactly one item.\n",
    "\n",
    "The format string for floating point is `\"f\"` and it requires a 4 bytes buffer for unpack.\n",
    "\n",
    "For example:\n",
    "\n",
    "    a = pack('f', 2.5)  # a stores the binary representation of 2.5 in IEEE 754 format\n",
    "    unpack('f', a)      # unpacks the binary representation of a into \"a\" into a IEEE 754 float"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float_a = 2.5\n",
    "binary_a = pack('f', float_a)\n",
    "int_a = int.from_bytes(binary_a, byteorder='little')\n",
    "zedBoard.write_reg(??, int_a)\n",
    "zedBoard.write_reg(??, int_a)\n",
    "\n",
    "float_b = -0.24\n",
    "binary_b = pack('f', float_b)\n",
    "int_b = int.from_bytes(binary_b, byteorder='little')\n",
    "zedBoard.write_reg(??, int_b)\n",
    "zedBoard.write_reg(??, int_b)\n",
    "\n",
    "int_c = zedBoard.read_reg(??)[1][0]\n",
    "byte_c = int_c.to_bytes(length=4, byteorder='little')\n",
    "float_c = unpack('f', byte_c)[0]\n",
    "\n",
    "int_c_hls = zedBoard.read_reg(??)[1][0]\n",
    "byte_c_hls = int_c_hls.to_bytes(length=4, byteorder='little')\n",
    "float_c_hls = unpack('f', byte_c_hls)[0]\n",
    "\n",
    "print(\"SOFTWARE -> {:.9f} = {:.6f} / {:.6f}\".format(float_a/float_b, float_a, float_b))\n",
    "print(\"FLOPOCO  -> {:.9f} = {:.6f} / {:.6f}\".format(float_c, float_a, float_b))\n",
    "print(\"HLS      -> {:.9f} = {:.6f} / {:.6f}\".format(float_c_hls, float_a, float_b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Challenge: Flopoco vs. HLS\n",
    "\n",
    "Now we can check if the results from our HLS divisor IP are correct and compare them with the Flopoco block.\n",
    "To do so we must first load the operands in the correct ComBlock registers, read the result, and cast it to float to compare.\n",
    "\n",
    "You should also check how both handle infinite cases, underflow, and not-a-number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
