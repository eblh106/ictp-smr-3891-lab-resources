![LOGO](./.img/LOGO.png)

# Joint ICTP-IAEA School on Systems-on-Chip based on FPGA for Scientific Instrumentation and Reconfigurable Computing | (smr 3891)

## Link to [event.](https://indico.ictp.it/event/10225/)
## Link to the [Wiki.](https://gitlab.com/ictp-mlab/smr-3891/-/wikis/home)


Source code for lab activities:

0. [Use of VMs and User environment](Labs/Lab0_Before_Starting/)

1. [Lab 1: Hello World and GPIO In/Out](Labs/Lab1_GPIO/)

2. [Lab 2: ComBlock and RTL instantiation](Labs/Lab2_ComBlock/)

3. [Lab 3: SoC-FPGA Development Framework: UDMA & Jupyter Notebook](Labs/Lab3_UDMA/)

4. [Lab 4: SoC-FPGA DAQ system and TCL](Labs/Lab4_ADC_DAC/)

5. [Project 1: Digital Pulse Processing for Isotope Identification](Labs/LabProject/)

Source lab Demos:

1. [FreeRTOS Tutorial](https://drive.google.com/drive/folders/1yC4GTRdH0sxpIo5VO5MzUyG1eaI8zuvF)